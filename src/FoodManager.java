import java.util.*;

public class FoodManager
{
    /**
     *  CLASSNAME:      FoodManager
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Contain the main program
     *  FIRST CREATED:  23/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  METHODS:
     *      - main
     *      - runMenu
     *      - addFood
     *          - createMeat
     *          - createGrain
     *          - createFruit
     *          - createVegetable
     *      - removeFood
     *      - displayContents
     *      - findExpired
     *      - readStorage
     *          - processStorage
     *          - processFood
     *      - writeStorage
     **/

    public static void main(String[] args)
    {
        // NAME:        main
        // PURPOSE:     Run the menu
        // ASSERTION:   Should only exit when user selects exit
        // IMPORTS:     args (String array)
        // EXPORTS:     none

        runMenu();
    }

    public static void runMenu()
    {
        // NAME:        runMenu
        // PURPOSE:     Display menu and run the appropriate methods
        // ASSERTION:   Should only exit when use selects exit
        // IMPORTS:     none
        // EXPORTS:     none

        FoodStorage foodStorage = new FoodStorage();
        String[] menuOptions = new String[8];
        boolean exit = false;

        // Create prompt and options
        menuOptions[0] = "Please select an option:";
        menuOptions[1] = "    1. Add food";
        menuOptions[2] = "    2. Remove food";
        menuOptions[3] = "    3. Display contents";
        menuOptions[4] = "    4. Find expired";
        menuOptions[5] = "    5. Read in storage";
        menuOptions[6] = "    6. Write out storage";
        menuOptions[7] = "    7. Exit";

        // Loop until exit is chosen
        while (! exit)
        {
            try
            {
                // Case to determine which method to use
                switch (Menu.getUserInput(menuOptions))
                {
                    case 1:
                        // Add food
                        foodStorage = addFood(foodStorage);
                        exit = false;
                        break;
                    case 2:
                        // Remove food
                        foodStorage = removeFood(foodStorage);
                        exit = false;
                        break;
                    case 3:
                        // Display contents
                        displayContents(foodStorage);
                        exit = false;
                        break;
                    case 4:
                        // Find expired
                        findExpired(foodStorage);
                        exit = false;
                        break;
                    case 5:
                        // Read in from file
                        foodStorage = readStorage(foodStorage);
                        exit = false;
                        break;
                    case 6:
                        // Write to file
                        writeStorage(foodStorage);
                        exit = false;
                        break;
                    case 7:
                        // Exit
                        System.out.println("Exiting...");
                        exit = true;
                }
            }
            // For any errors from the submodules or objects
            catch (IllegalArgumentException e)
            {
                // Print the error message
                System.out.println(e.getMessage());

                // Set exit to false so that use goes back to the main menu
                exit = false;
            }
        }
    }

    public static FoodStorage addFood(FoodStorage foodStorage)
    {
        // NAME:        addFood
        // PURPOSE:     Add food to storage facility
        // ASSERTION:   Should add food to storage facility if user inputs are correct
        // IMPORTS:     foodStorage (FoodStorage)
        // EXPORTS:     foodStorage (FoodStorage)

        Food newFood;
        boolean validFood, inExit;
        String outputMsg, choice = "";
        String[] foodMenuOptions = new String[6];

        // If have not created storage facilities
        if (! foodStorage.getConstructFromFile())
        {
            throw new IllegalArgumentException("Invalid Storage: Cannot add food until read from file");
        }
        else
        {
            validFood = false;
            inExit = false;

            // Create food menu
            foodMenuOptions[0] = "Please select food type:";
            foodMenuOptions[1] = "    1. Meat";
            foodMenuOptions[2] = "    2. Grain";
            foodMenuOptions[3] = "    3. Fruit";
            foodMenuOptions[4] = "    4. Vegetable";
            foodMenuOptions[5] = "    5. Exit to main menu";

            // While exit is not selected and food inputs is not true
            while ((! inExit) && (! validFood))
            {
                try
                {
                    // Get user input
                    switch (Menu.getUserInput(foodMenuOptions))
                    {
                        case 1:
                            // Add meat
                            choice = "Add meat";
                            newFood = createMeat();
                            foodStorage.addItem(newFood);
                            validFood = true;
                            break;
                        case 2:
                            // Add grain
                            choice = "Add grain";
                            newFood = createGrain();
                            foodStorage.addItem(newFood);
                            validFood = true;
                            break;
                        case 3:
                            // Add fruit
                            choice = "Add fruit";
                            newFood = createFruit();
                            foodStorage.addItem(newFood);
                            validFood = true;
                            break;
                        case 4:
                            // Add vegetable
                            choice = "Add vegetable";
                            newFood = createVegetable();
                            foodStorage.addItem(newFood);
                            validFood = true;
                            break;
                        case 5:
                            // Exit
                            choice = "Exit";
                            inExit = true;
                            break;
                    }
                }
                // For any error messages from methods or objects
                catch (IllegalArgumentException e)
                {
                    // Display error message
                    System.out.println(e.getMessage());

                    // Set exit to false so that menu loops
                    inExit = false;
                }
            }

            // Display message to show that action has been done successfully
            outputMsg = "==============================\n"
                        + "Finish action: " + choice + "\n"
                        + "==============================\n";
            System.out.println(outputMsg);
        }

        return foodStorage;
    }

    public static Food createMeat()
    {
        // NAME:        createMeat
        // PURPOSE:     Create a meat object
        // ASSERTION:   Should successfully create meat object when user inputs are valid
        // IMPORTS:     none
        // EXPORTS:     newMeat (Food)

        Food newMeat;
        String[] prompt, errorMsg;
        String name, cut, packageType;
        double tempStorage, weight;
        Date useBy = new Date();
        boolean dateValid = false;

        // Create prompts and error messages for adding meat
        prompt = new String[6];
        errorMsg = new String[6];

        prompt[0] = "Enter meat name:";
        prompt[1] = "Enter meat cut:";
        prompt[2] = "Enter meat weight";
        prompt[3] = "Enter meat storage temperature:";
        prompt[4] = "Enter use by date (dd/mm/yyyy):";
        prompt[5] = "Enter meat package type:";

        errorMsg[0] = "Error: Name is not valid\nPlease try again";
        errorMsg[1] = "Error: Cut is not valid\nPlease try again";
        errorMsg[2] = "Error: Weight is not valid\nPlease try again";
        errorMsg[3] = "Error: Storage temperature is not valid\nPlease try again";
        errorMsg[4] = "Error: Date is not valid\nPlease try again";
        errorMsg[5] = "Error: Package type is not valid\nPlease try again";

        // Get input from user
        name = Input.string(prompt[0], errorMsg[0]);
        cut = Input.string(prompt[1], errorMsg[1]);
        weight = Input.real(prompt[2], errorMsg[2], 0.2, 5.0);
        tempStorage = Input.real(prompt[3], errorMsg[3]);

        // Loop until date is valid
        do
        {
            try
            {
                useBy = new Date(Input.string(prompt[4], errorMsg[4]));
                dateValid = true;
            }
            catch (IllegalArgumentException e)
            {
                System.out.println(errorMsg[4]);
                dateValid = false;
            }
        } while (! dateValid);

        packageType = Input.string(prompt[5], errorMsg[5]);

        // Create meat object
        newMeat = new Meat(name, tempStorage, packageType, cut, weight, useBy);
        return newMeat;
    }

    public static Food createGrain()
    {
        // NAME:        createGrain
        // PURPOSE:     Create a grain object
        // ASSERTION:   Should successfully create grain object when user inputs are valid
        // IMPORTS:     none
        // EXPORTS:     newGrain (Food)

        Food newGrain;
        String[] prompt, errorMsg;
        String name, type, packageType;
        double tempStorage, volume;
        Date expireBy = new Date();
        boolean dateValid = false;

        // Create prompts and error messages from adding grain
        prompt = new String[6];
        errorMsg = new String[6];

        prompt[0] = "Enter grain name:";
        prompt[1] = "Enter grain type:";
        prompt[2] = "Enter grain volume";
        prompt[3] = "Enter grain storage temperature:";
        prompt[4] = "Enter expire by date (dd/mm/yyyy):";
        prompt[5] = "Enter grain package type:";

        errorMsg[0] = "Error: Name is not valid\nPlease try again";
        errorMsg[1] = "Error: Type is not valid\nPlease try again";
        errorMsg[2] = "Error: Volume is not valid\nPlease try again";
        errorMsg[3] = "Error: Storage temperature is not valid\nPlease try again";
        errorMsg[4] = "Error: Date is not valid\nPlease try again";
        errorMsg[5] = "Error: Package type is not valid\nPlease try again";

        // Get input from user
        name = Input.string(prompt[0], errorMsg[0]);
        type = Input.string(prompt[1], errorMsg[1]);
        volume = Input.real(prompt[2], errorMsg[2], 0.2, 5.0);
        tempStorage = Input.real(prompt[3], errorMsg[3]);

        // Loop until date is valid
        do
        {
            try
            {
                expireBy = new Date(Input.string(prompt[4], errorMsg[4]));
                dateValid = true;
            }
            catch (IllegalArgumentException e)
            {
                System.out.println(errorMsg[4]);
                dateValid = false;
            }
        } while (! dateValid);

        packageType = Input.string(prompt[5], errorMsg[5]);

        // Create grain object
        newGrain = new Grain(name, tempStorage, packageType, type, volume, expireBy);
        return newGrain;
    }

    public static Food createFruit()
    {
        // NAME:        createFruit
        // PURPOSE:     Create a fruit object
        // ASSERTION:   Should successfully create fruit object when user inputs are valid
        // IMPORTS:     none
        // EXPORTS:     newFruit (Food)

        Food newFruit;
        String[] prompt, errorMsg;
        String name, type, packageType;
        double tempStorage;
        int numPieces;
        Date useBy = new Date();
        boolean dateValid = false;

        // Create prompts and error messages for adding fruit
        prompt = new String[6];
        errorMsg = new String[6];

        prompt[0] = "Enter fruit name:";
        prompt[1] = "Enter fruit type:";
        prompt[2] = "Enter number of pieces";
        prompt[3] = "Enter fruit storage temperature:";
        prompt[4] = "Enter use by date (dd/mm/yyyy):";
        prompt[5] = "Enter fruit package type:";

        errorMsg[0] = "Error: Name is not valid\nPlease try again";
        errorMsg[1] = "Error: Type is not valid\nPlease try again";
        errorMsg[2] = "Error: No. of pieces is not valid\nPlease try again";
        errorMsg[3] = "Error: Storage temperature is not valid\nPlease try again";
        errorMsg[4] = "Error: Date is not valid\nPlease try again";
        errorMsg[5] = "Error: Package type is not valid\nPlease try again";

        // Get input from user
        name = Input.string(prompt[0], errorMsg[0]);
        type = Input.string(prompt[1], errorMsg[1]);
        numPieces = Input.integer(prompt[2], errorMsg[2], 1, 20);
        tempStorage = Input.real(prompt[3], errorMsg[3]);

        // Loop until date is valid
        do
        {
            try
            {
                useBy = new Date(Input.string(prompt[4], errorMsg[4]));
                dateValid = true;
            }
            catch (IllegalArgumentException e)
            {
                System.out.println(errorMsg[4]);
                dateValid = false;
            }
        } while (! dateValid);

        packageType = Input.string(prompt[5], errorMsg[5]);

        // Create fruit object
        newFruit = new Fruit(name, tempStorage, packageType, type, numPieces, useBy);
        return newFruit;
    }

    public static Food createVegetable()
    {
        // NAME:        createVegetable
        // PURPOSE:     Create a vegetable object
        // ASSERTION:   Should successfully crete vegetable object when user inputs are valid
        // IMPORTS:     none
        // EXPORTS:     newVegetable (Food)

        Food newVegetable;
        String[] prompt, errorMsg;
        String name, packageType;
        double tempStorage, weight;
        Date expireBy = new Date();
        boolean dateValid = false;

        // Create prompts and error messages for adding vegetable
        prompt = new String[5];
        errorMsg = new String[5];

        prompt[0] = "Enter vegetable name:";
        prompt[1] = "Enter vegetable weight";
        prompt[2] = "Enter vegetable storage temperature:";
        prompt[3] = "Enter expire by date (dd/mm/yyyy):";
        prompt[4] = "Enter vegetable package type:";

        errorMsg[0] = "Error: Name is not valid\nPlease try again";
        errorMsg[1] = "Error: Weight is not valid\nPlease try again";
        errorMsg[2] = "Error: Storage temperature is not valid\nPlease try again";
        errorMsg[3] = "Error: Date is not valid\nPlease try again";
        errorMsg[4] = "Error: Package type is not valid\nPlease try again";

        // Get input from user
        name = Input.string(prompt[0], errorMsg[0]);
        weight = Input.real(prompt[1], errorMsg[1], 0.2, 5.0);
        tempStorage = Input.real(prompt[2], errorMsg[2]);

        // Loop until date is valid
        do
        {
            try
            {
                expireBy = new Date(Input.string(prompt[3], errorMsg[3]));
                dateValid = true;
            }
            catch (IllegalArgumentException e)
            {
                System.out.println(errorMsg[3]);
                dateValid = false;
            }
        } while (! dateValid);

        packageType = Input.string(prompt[4], errorMsg[4]);

        // Create vegetable object
        newVegetable = new Vegetable(name, tempStorage, packageType, weight, expireBy);
        return newVegetable;
    }

    public static FoodStorage removeFood(FoodStorage foodStorage)
    {
        // NAME:        removeFood
        // PURPOSE:     Remove food from storage facility
        // ASSERTION:   Should successfully remove food unless it doesnt exist
        // IMPORTS:     foodStorage (FoodStorage)
        // EXPORTS:     foodStorage (FoodStorage)

        String prompt, errorMsg, location, outputMsg;

        // If have not created storage facilities
        if (! foodStorage.getConstructFromFile())
        {
            throw new IllegalArgumentException("Invalid Storage: Cannot remove food until read from file");
        }
        else
        {
            // Create prompt and error message for user input
            prompt = "Enter the index of the item in the storage you want to remove (x,y):\n"
                     + "To go back to main menu, enter \"e\"";
            errorMsg = "Error: Location entered is not valid\nPlease try again";

            // Get location
            location = Input.string(prompt, errorMsg);

            // If location is not "e"
            if (! location.equals("e"))
            {
                // Remove item
                outputMsg = foodStorage.removeItem(location);

                // Display message to show that action has been done successfully
                outputMsg += "\n\n==============================\n"
                           + "Finish action: Remove item\n"
                           + "==============================\n";
                System.out.println(outputMsg);
            }
        }

        return foodStorage;
    }

    public static void displayContents(FoodStorage foodStorage)
    {
        // NAME:        displayContents
        // PURPOSE:     Display the contents of a selected storage facility
        // ASSERTION:   Should exit if storage facility cannot be found
        // IMPORTS:     foodStorage (FoodStorage)
        // EXPORTS:     none

        String prompt, errorMsg, storageName, toDisplay, outputMsg;

        // If have not created storage facilities
        if (! foodStorage.getConstructFromFile())
        {
            throw new IllegalArgumentException("Invalid Storage: Cannot display contents until read from file");
        }
        else
        {
            // Create prompt and error message for user input
            prompt = "Enter the name of the storage facility to display:";
            errorMsg = "Error: Name entered is not valid\n"
                     + "Please try again";

            // Get storage facility name
            storageName = Input.string(prompt, errorMsg);

            // Get the contents in the storage facility
            toDisplay = foodStorage.displayStorage(storageName);
            System.out.println(toDisplay);

            // Display message to show that action has been done successfully
            outputMsg = "==============================\n"
                        + "Finish action: Display contents\n"
                        + "==============================\n";
            System.out.println(outputMsg);
        }
    }

    public static void findExpired(FoodStorage foodStorage)
    {
        // NAME:        findExpired
        // PURPOSE:     Find the location of food items in storage that has expired
        // ASSERTION:   Display expired food and return to main menu
        // IMPORTS:     foodStorage (FoodStorage)
        // EXPORTS:     none

        String outputMsg;

        // If have not created storage facilities
        if (! foodStorage.getConstructFromFile())
        {
            throw new IllegalArgumentException("Invalid Storage: Cannot find expired until read from file");
        }
        else
        {
            // Find expired food
            System.out.println(foodStorage.findExpired());

            // Display message to show that action has been done successfully
            outputMsg = "==============================\n"
                        + "Finish action: Find expired items\n"
                        + "==============================\n";
            System.out.println(outputMsg);
        }
    }

    public static FoodStorage readStorage(FoodStorage foodStorage)
    {
        // NAME:        readStorage
        // PURPOSE:     Read from file and construct storage facility and food
        // ASSERTION:   Should be able to construct storage facility unless file is not a valid format
        // IMPORTS:     foodStorage (FoodStorage)
        // EXPORTS:     foodStorage (FoodStorage)

        String prompt, errorMsg, filename, outputMsg;
        String[] fileContents;

        // If have not created storage facilities
        if (foodStorage.getConstructFromFile())
        {
            throw new IllegalArgumentException("Invalid Storage: Already read from file");
        }
        else
        {
            // Create prompt and error message for user input
            prompt = "Enter the filename to read from:";
            errorMsg = "Error: Filename entered is not valid\n"
                     + "Please try again";

            // Get filename and file contents
            filename = Input.string(prompt, errorMsg);
            fileContents = FileIO.readFileContents(filename);

            // Process storage and food
            foodStorage = processStorage(fileContents, foodStorage);
            foodStorage = processFood(fileContents, foodStorage);

            // Display message to show that action has been done successfully
            outputMsg = "==============================\n"
                        + "Finish action: Read storage\n"
                        + "==============================\n";
            System.out.println(outputMsg);
        }

        return foodStorage;
    }

    public static FoodStorage processStorage(String[] inFileContents, FoodStorage foodStorage)
    {
        // NAME:        processStorage
        // PURPOSE:     Read from file and construct storage
        // ASSERTION:   Should be able to create storage facility unless file is not a valid format
        // IMPORTS:     inFileContents (String array), foodStorage (FoodStorage)
        // EXPORTS:     foodStorage (FoodStorage)

        Storage[][] newStorage;
        String[] splitLine;
        String storageType;
        int storageSize;

        // If the file length is less than 3
        if (inFileContents.length < 3)
        {
            throw new IllegalArgumentException("Invalid Storage: Input file does not contain enough information to construct storage facility");
        }
        else
        {
            newStorage = new Storage[3][];

            // For the first 3 lines in file
            for (int i = 0; i < 3; i ++)
            {
                // Split at ","
                splitLine = inFileContents[i].split(",");

                // If theres only one entry
                if (splitLine.length < 1)
                {
                    throw new IllegalArgumentException("Invalid Storage: Input file does not contain enough information to construct storage facility");
                }
                else
                {
                    // Get name and size of storage
                    storageType = splitLine[0];
                    storageSize = Integer.parseInt(splitLine[1]);

                    // Create an array of Freezer, Fridge or Pantry
                    if (storageType.equals("Freezer"))
                    {
                        newStorage[i] = new Freezer[storageSize];
                        for (int j = 0; j < newStorage[i].length; j++)
                        {
                            newStorage[i][j] = new Freezer(storageType);
                        }
                    }
                    else if (storageType.equals("Fridge"))
                    {
                        newStorage[i] = new Fridge[storageSize];
                        for (int j = 0; j < newStorage[i].length; j++)
                        {
                            newStorage[i][j] = new Fridge(storageType);
                        }
                    }
                    else if (storageType.equals("Pantry"))
                    {
                        newStorage[i] = new Pantry[storageSize];
                        for (int j = 0; j < newStorage[i].length; j++)
                        {
                            newStorage[i][j] = new Pantry(storageType);
                        }
                    }
                }
            }
            foodStorage = new FoodStorage(newStorage);
        }

        return foodStorage;
    }

    public static FoodStorage processFood(String[] inFileContents, FoodStorage foodStorage)
    {
        // NAME:        processFood
        // PURPOSE:     Read from file and construct food objects
        // ASSERTION:   Create an array of food objects, skipping invalid entries
        // IMPORTS:     inFileContents (String array), foodStorage (FoodStorage)
        // EXPORTS:     foodStorage (FoodStorage)

        int countValid, countInvalid;
        String[] splitLine;
        String outputMsg;

        String typeFood, name, cut, type, packageType;
        double weight, volume, tempStorage;
        Date useBy, expireBy;
        int numPieces;

        Food newMeat, newGrain, newFruit, newVegetable;

        countValid = 0;
        countInvalid = 0;

        // For each line in file
        for (int i = 0; i < inFileContents.length; i++)
        {
            try
            {
                // Split the line at ","
                splitLine = inFileContents[i].split(",");

                // If split line has more than 2 entries
                if (splitLine.length > 2)
                {
                    // Determine which food object to create
                    typeFood = splitLine[0];

                    if (typeFood.equals("Meat"))
                    {
                        // For meat
                        name = splitLine[1];
                        cut = splitLine[2];
                        weight = Double.parseDouble(splitLine[3]);
                        tempStorage = Double.parseDouble(splitLine[4]);
                        useBy = new Date(splitLine[5]);
                        packageType = splitLine[6];
                        newMeat = new Meat(name, tempStorage, packageType, cut, weight, useBy);
                        foodStorage.addItem(newMeat);
                        countValid += 1;
                    }
                    else if (typeFood.equals("Grain"))
                    {
                        // For grain
                        name = splitLine[1];
                        type = splitLine[2];
                        volume = Double.parseDouble(splitLine[3]);
                        tempStorage = Double.parseDouble(splitLine[4]);
                        expireBy = new Date(splitLine[5]);
                        packageType = splitLine[6];
                        newGrain = new Grain(name, tempStorage, packageType, type, volume, expireBy);
                        foodStorage.addItem(newGrain);
                        countValid += 1;
                    }
                    else if (typeFood.equals("Fruit"))
                    {
                        // For fruit
                        name = splitLine[1];
                        type = splitLine[2];
                        numPieces = Integer.parseInt(splitLine[3]);
                        tempStorage = Double.parseDouble(splitLine[4]);
                        useBy = new Date(splitLine[5]);
                        packageType = splitLine[6];
                        newFruit = new Fruit(name, tempStorage, packageType, type, numPieces, useBy);
                        foodStorage.addItem(newFruit);
                        countValid += 1;
                    }
                    else if (typeFood.equals("Vegetable"))
                    {
                        // For vegetable
                        name = splitLine[1];
                        weight = Double.parseDouble(splitLine[2]);
                        tempStorage = Double.parseDouble(splitLine[3]);
                        expireBy = new Date(splitLine[4]);
                        packageType = splitLine[5];
                        newVegetable = new Vegetable(name, tempStorage, packageType, weight, expireBy);
                        foodStorage.addItem(newVegetable);
                        countValid += 1;
                    }
                }
            }
            // For any errors caught
            catch (IllegalArgumentException e)
            {
                // Increment invalid count
                countInvalid += 1;

                // Display message
                System.out.println(e.getMessage());
                System.out.println("Entry at line " + (i + 1) + " is invalid, skipping...\n");
            }
        }

        // Display valid and invalid numbers
        outputMsg = "No. of valid entries: " + countValid + "\n"
                    + "No. of invalid entries: " + countInvalid;
        System.out.println(outputMsg);

        return foodStorage;
    }

    public static void writeStorage(FoodStorage foodStorage)
    {
        // NAME:        writeStorage
        // PURPOSE:     Write the contents of foodStorage into a file
        // ASSERTION:   Export storage and food objects to a file in csv form
        // IMPORTS:     foodStorage (FoodStorage)
        // EXPORTS:     none

        String prompt, errorMsg, outFilename, outStorage, outputString;
        String[] outFileContents;

        // If have not created storage facilities
        if (! foodStorage.getConstructFromFile())
        {
            throw new IllegalArgumentException("Invalid Storage: Cannot write to file until read from file");
        }
        else
        {
            // Create prompt and error message for user input
            prompt = "Enter the filename to write to:";
            errorMsg = "Error: Filename entered is not valid\n"
                     + "Please try again";

            // Get output filename
            outFilename = Input.string(prompt, errorMsg);
            
            // Get output file contents
            outFileContents = foodStorage.exportFoodStorage();

            // Save the file contents
            FileIO.saveFileContents(outFilename, outFileContents);

            outputString = "==============================\n"
                         + "Successfully saved " + outFileContents.length + " lines\n"
                         + "==============================\n\n";
            System.out.println(outputString);
        }
    }
}
