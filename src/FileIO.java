import java.io.*;

public class FileIO
{
    /**
     *  CLASSNAME:      FileIO
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        A File Class to write and read files
     *  FIRST CREATED:  11/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  PUBLIC METHODS:
     *      - printFileLine
     *      - readFileContents
     *      - saveFileContents (New File)
     *
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateFile
     *      - calcFileLines
     *      - compareArray
     *      - readFile
     *      - writeFile
     **/

    public static String[] readFileContents(String inFilename)
    {
        // NAME:        readFileContents
        // PURPOSE:      Open file and copy each line to an array
        // ASSERTION:   File contents will be copied to an array and returned
        // IMPORTS:     inFilename (String)
        // EXPORTS:     fileContents (String array)

        String[] fileContents;

        // If inFilename is not valid
        if (! validateString(inFilename))
        {
            throw new IllegalArgumentException("Invalid Filename: Filename is either null or empty");
        }
        else
        {
            fileContents = readFile(inFilename);
        }

        return fileContents;
    }

    public static void saveFileContents(String newFilename, String[] inFileContents)
    {
        // NAME:        saveFileContents (New File)
        // PURPOSE:     Write the contents of the file to a new file
        // ASSERTION:   Create a new file with fileContents array
        // IMPORTS:     newFilename (String)
        // EXPORTS:     none

        // If newFilename is not valid
        if (! validateString(newFilename))
        {
            throw new IllegalArgumentException("Invalid Filename: Filename is either null or empty");
        }
        else
        {
            writeFile(newFilename, inFileContents);
        }
    }

    public static void printFileLine(int userLineNum, String[] inFileContents)
    {
        // NAME:        printFileLine
        // PURPOSE:     Print a line from the file
        // ASSERTION:   Output the specified line of the file to the screen
        // IMPORTS:     userLineNum (int), inFileContents (String array)
        // EXPORTS:     none

        // If userLineNum is 0 or negative
        if (userLineNum < 1)
        {
            throw new IllegalArgumentException("Invalid File: Line number selected is lower than 1");
        }
        // If userLineNum greater than the number of lines in the file
        else if (userLineNum > inFileContents.length)
        {
            throw new IllegalArgumentException("Invalid File: Line number selected is higher than number of lines in file");
        }
        else
        {
            System.out.println(inFileContents[userLineNum - 1]);
        }
    }

    private static boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check to see if string is not null and not empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     valid (boolean)

        boolean isValid;
        isValid= ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private static boolean validateFile(String inFilename)
    {
        // NAME:        validateFile
        // PURPOSE:     Check to see if file is not empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inFilename (String)
        // EXPORTS:     valid (boolean)

        boolean isValid;
        isValid = (calcFileLines(inFilename) != 0);
        return isValid;
    }

    private static int calcFileLines(String inFilename)
    {
        // NAME:        calcFileLines
        // PURPOSE:     Count number of lines in a file
        // ASSERTION:   Return numLines after counting lines
        // IMPORTS:     inFilename (String)
        // EXPORTS:     numLines (int)

        FileInputStream fileStream = null;
        InputStreamReader rdr;
        BufferedReader buffRdr;

        String line;
        int numLines = 0;

        try
        {
            // Open file
            fileStream = new FileInputStream(inFilename);
            rdr = new InputStreamReader(fileStream);
            buffRdr = new BufferedReader(rdr);

            // While line is not EOF
            while (buffRdr.readLine() != null)
            {
                numLines += 1;
            }
            fileStream.close();
        }
        // Catch IO errors
        catch (IOException e)
        {
            // If file stream is still open
            if (fileStream != null)
            {
                // Close it
                try
                {
                    fileStream.close();
                }
                catch (IOException ex)
                {

                }
            }
        }

        return numLines;
    }

    private static boolean compareArray(String[] arrayOne, String[] arrayTwo)
    {
        // NAME:        compareArray
        // PURPOSE:     Compare the contents of one array to another
        // ASSERTION:   Return true or false
        // IMPORTS:     arrayOne (String array), arrayTwo (String array)
        // EXPORTS:     equals (boolean)

        boolean equals = true;
        int i;
        i = 0;

        // Check if length is the same on the two arrays
        if (arrayOne.length == arrayTwo.length)
        {
            // While equals is true, do until it's false 
            while ((equals) && (i < arrayOne.length))
            {
                equals = (arrayOne[i].equals(arrayTwo[i]));
                i += 1;
            }
        }

        return equals;
    }

    private static String[] readFile(String inFilename)
    {
        // NAME:        readFile
        // PURPOSE:     Open file and save it to an array
        // ASSERTION:   Return file Array
        // IMPORTS:     inFilename (String)
        // EXPORTS:     arrayFile (String array)

        FileInputStream fileStream = null;
        InputStreamReader rdr;
        BufferedReader buffRdr;

        String line;
        String[] arrayFile;
        int numLines;

        numLines = calcFileLines(inFilename);
        arrayFile = new String[numLines];

        try
        {
            // Open file
            fileStream = new FileInputStream(inFilename);
            rdr = new InputStreamReader(fileStream);
            buffRdr = new BufferedReader(rdr);

            // For each line in the file
            for (int i = 0; i < numLines; i++)
            {
                // Assign line to array
                arrayFile[i] = buffRdr.readLine();
            }

            fileStream.close();
        }
        catch (IOException e)
        {
            if (fileStream != null)
            {
                try
                {
                    fileStream.close();
                }
                catch (IOException ex)
                {

                }
            }
        }
        return arrayFile;
    }

    private static void writeFile(String inFilename, String[] inFileContents)
    {
        // NAME:        writeFile
        // PURPOSE:     Write the contents of an array to a file
        // ASSERTION:   Create a file with array contents
        // IMPORTS:     inFilename (String), inFileContents (String array)
        // EXPORTS:     none

        FileOutputStream fileStream = null;
        PrintWriter pw;

        try
        {
            // Open file
            fileStream = new FileOutputStream(inFilename);
            pw = new PrintWriter(fileStream);

            // For each elements in array
            for (int i = 0; i < inFileContents.length; i++)
            {
                // Print line to file
                pw.println(inFileContents[i]);
            }

            pw.close();
        }
        catch (IOException e)
        {
            if (fileStream != null)
            {
                try
                {
                    fileStream.close();
                }
                catch (IOException ex)
                {

                }
            }
        }
    }
}
