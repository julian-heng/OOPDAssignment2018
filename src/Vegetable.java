import java.util.*;

public class Vegetable extends Food implements IFood
{
    /**
     *  CLASSNAME:      Vegetable
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Create a vegetable object containing characteristics of the food item
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASSFIELDS:
     *      - weight (double)
     *      - expireBy (Date)
     *      - expired (boolean)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor
     *      - Copy Constructor
     *      - calcExpire
     *      - toCSV
     *      - setWeight
     *      - setExpireBy
     *      - getWeight
     *      - getExpireBy
     *      - getItemTypeStr
     *      - getItemType
     *      - clone
     *      - equals
     *      - toString
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateReal
     *      - validateObject
     *      - validateDate
     *      - realCompare
     **/

    // Declare class fields
    private String cut;
    private double weight;
    private Date expireBy;
    private boolean expired;

    // Constructors
    public Vegetable()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a vegetable object with placeholder values
        // ASSERTION:   Default vegetable object will be created
        // IMPORTS:     none
        // EXPORTS:     none

        // Call super class
        super();

        // Set default values
        weight = 10.0;
        expireBy = new Date();
        expired = validateDate(expireBy);
    }

    public Vegetable(String inName, double inTempStorage, String inPackageType, double inWeight, Date inExpireBy)
    {
        // NAME:        Alternate Constructor
        // PURPOSE:     Create a vegetable object with imports
        // ASSERTION:   Vegetable object with imports will be created
        // IMPORTS:     inName (String), intempStorage (double), inPackageType (String), inWeight (double), inExpireBy (Date)
        // EXPORTS:     none

        // Call super class
        super(inName, inTempStorage, inPackageType);

        // Call setters
        setWeight(inWeight);
        setExpireBy(inExpireBy);
    }

    public Vegetable(Vegetable inVegetable)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Copy the contents of the vegetable import into a new object
        // ASSERTION:   Copy inVegetable object to new vegetable object
        // IMPORTS:     inVegetable (Vegetable)
        // EXPORTS:     none

        // Call super class
        super(inVegetable);

        // Call getters in inVegetable
        weight = inVegetable.getWeight();
        expireBy = inVegetable.getExpireBy();
    }

    // Mutators
    @Override
    public String toCSV()
    {
        // NAME:        toCSV
        // PURPOSE:     Combine the contents of this object into a csv line
        // ASSERTION:   Return csv line from this object
        // IMPORTS:     none
        // EXPORTS:     csvLine (String)

        String csvLine;

        // Assemble csv line
        csvLine = getItemTypeStr() + "," 
                + super.getName() + "," 
                + weight + "," 
                + super.getTempStorage() + "," 
                + expireBy.toString() + "," 
                + super.getPackageType();
        return csvLine;
    }

    @Override
    public boolean calcExpire(Date today)
    {
        // NAME:        calcExpire
        // PURPOSE:     Determine if the object expire date has passed current date
        // ASSERTION:   Return true or false
        // IMPORTS:     today (Date)
        // EXPORTS:     isExpire (boolean)

        boolean isExpire = false;
        isExpire = today.isBehind(expireBy);
        return isExpire;
    }

    public void setWeight(double inWeight)
    {
        // NAME:        setWeight
        // PURPOSE:     Change the weight variable to inWeight
        // ASSERTION:   weight will be replaced by inWeight
        // IMPORTS:     inWeight (double)
        // EXPORTS:     none

        // If inWeight is not in between 0.2 and 5.0
        if (! validateReal(inWeight))
        {
            throw new IllegalArgumentException("Invalid Vegetable: Cannot set weight, is less than 0");
        }
        else
        {
            weight = inWeight;
        }
    }

    // Setters
    public void setExpireBy(Date inExpireBy)
    {
        // NAME:        setExpireBy
        // PURPOSE:     Change the expireBy date object to inExpireBy
        // ASSERTION:   expireBy will be replaced by inExpireBy
        // IMPORTS:     inExpireBy (Date)
        // EXPORTS:     none

        // If either inExpireBy is null or date is in the past
        if ((! validateObject(inExpireBy)) || (validateDate(inExpireBy)))
        {
            throw new IllegalArgumentException("Invalid Vegetable: Cannot set use by date, not valid");
        }
        else
        {
            expireBy = new Date(inExpireBy);
            expired = validateDate(expireBy);
        }
    }

    // Getters
    public double getWeight() { return weight; }
    public Date getExpireBy() { return new Date(expireBy); }
    public boolean getExpired() { return expired; }

    @Override
    public String getItemTypeStr() { return "Vegetable"; }

    @Override
    public char getItemType() { return 'V'; }

    @Override
    public Vegetable clone()
    {
        // NAME:        clone
        // PURPOSE:     Clone this object
        // ASSERTION:   A clone will be made
        // IMPORTS:     none
        // EXPORTS:     cloneVegetable (Vegetable)

        Vegetable cloneVegetable = new Vegetable(this);
        return cloneVegetable;
    }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Compare inObj to current object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a vegetable object
        if (inObj instanceof Vegetable)
        {
            Vegetable inVegetable = (Vegetable)inObj;
            isEquals = (
                (
                    super.equals(inVegetable)
                ) && (
                    realCompare(weight, inVegetable.getWeight())
                ) && (
                    expireBy.equals(inVegetable.getExpireBy())
                ) && (
                    expired == inVegetable.getExpired()
                )
            );
        }
        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Convert all the contents of vegetable object to a string
        // ASSERTION:   Return a string with all characteristics to this object
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = "Item type: " + getItemTypeStr() + "\n"
            + super.toString() + "\n"
            + "Weight: " + weight + "\n"
            + "Expire by: " + expireBy.toString() + "\n"
            + "Expired: " + expired;
        return str;
    }

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     none

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean validateReal(double inReal)
    {
        // NAME:        validateReal
        // PURPOSE:     Check if inReal is more than 0.2 but less than 5.0
        // ASSERTION:   Return true or false
        // IMPORTS:     inReal (double)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (
            (
                (inReal > 0.2) || (realCompare(inReal, 0.2))
            ) && (
                (inReal < 5.0) || (realCompare(inReal, 5.0))
            )
        );
        return isValid;
    }

    private boolean validateObject(Object inObject)
    {
        // NAME:        validateObject
        // PURPOSE:     Check if inObject is null
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (inObject != null);
        return isValid;
    }

    private boolean validateDate(Date inExpireBy)
    {
        // NAME:        validateDate
        // PURPOSE:     Check if date is not in the past
        // ASSERTION:   Return true or false
        // IMPORTS:     inExpireBy (Date)
        // EXPORTS:     isValid (boolean)

        Date today = new Date().setToday();
        boolean isValid = false;
        isValid = today.isBehind(inExpireBy);
        return isValid;
    }

    private boolean realCompare(double num1, double num2)
    {
        // NAME:        realCompare
        // PURPOSE:     Check if two real numbers are the same value
        // ASSERTION:   Return true or false
        // IMPORTS:     num1 (double), num2 (double)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;
        isEquals = (Math.abs(num1 - num2) < 0.0000001);
        return isEquals;
    }
}
