import java.util.*;

public class Fridge extends Storage
{
    /**
     *  CLASSNAME:      Fridge
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Storage facility to store items with temperatures -2.0 to 6.0
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASS FIELDS:
     *      - LOWERTEMP (double Constant)
     *      - UPPERTEMP (double Constant)
     *      - name (String)
     *      - item (Food)
     *      - hasItem (boolean)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor (No Item)
     *      - Alternate Constructor (Item)
     *      - Copy Constructor
     *      - isExpired
     *      - toCSV
     *      - canAdd
     *      - setName
     *      - setItem
     *      - getLower
     *      - getUpper
     *      - getName
     *      - getItem
     *      - getHasItem
     *      - equals
     *      - toString
     *      - clone
     *      - placeholder
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateObject
     *      - validateTemp
     *      - realCompare
     **/

    // Declare class fields
    public static final double LOWERTEMP = -2.0, UPPERTEMP = 6.0;
    private String name;
    private Food item;
    private boolean hasItem;

    // Constructors
    public Fridge()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a Fridge with placeholder values
        // ASSERTION:   Fridge object with default values is created
        // IMPORTS:     none
        // EXPORTS:     none

        // Set default values
        name = "Placeholder";
        item = new Meat();
    }

    public Fridge(String inName)
    {
        // NAME:        Alternate Constructor (No Item)
        // PURPOSE:     Create a Fridge with imports
        // ASSERTION:   Fridge object with import value is created
        // IMPORTS:     inName (String)
        // EXPORTS:     none

        // Call setters to set values
        setName(inName);

        // Create a placeholder food item
        item = new Meat();
    }

    public Fridge(String inName, Food inItem)
    {
        // NAME:        Alternate Constructor (Item)
        // PURPOSE:     Create a Fridge with imports
        // ASSERTION:   Fridge object with import value is created
        // IMPORTS:     inName (String), inItem (Food)
        // EXPORTS:     none

        // Call setters to set values
        setName(inName);
        setItem(inItem);
    }

    public Fridge(Fridge inFridge)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Copy the contents of inFridge to this object
        // ASSERTION:   Copy of inFridge will be created
        // IMPORTS:     inFridge (Fridge)
        // EXPORTS:     none

        // Call getters in inFridge
        name = inFridge.getName();
        item = inFridge.getItem();
        hasItem = inFridge.getHasItem();
    }

    public boolean isExpired()
    {
        // NAME:        isExpired
        // PURPOSE:     Check if the item in the Fridge is expired
        // ASSERTION:   Return true or false
        // IMPORTS:     none
        // EXPORTS:     isExpired (boolean)

        boolean isExpired = false;

        // Create a Date object with today's date
        Date today = new Date().setToday();

        // Call the calcExpired method in the item
        isExpired = (item.calcExpire(today));
        return isExpired;
    }

    @Override
    public String toCSV()
    {
        // NAME:        toCSV
        // PURPOSE:     Return the item in Fridge as a csv line
        // ASSERTION:   Call the toCSV method in the item and pass it through
        // IMPORTS:     none
        // EXPORTS:     itemCSV

        String itemCSV;
        itemCSV = item.toCSV();
        return itemCSV;
    }

    @Override
    public boolean canAdd(Food inFood)
    {
        // NAME:        canAdd
        // PURPOSE:     Check if the food item can be stored in the Fridge
        // ASSERTION:   Returns true or false
        // IMPORTS:     inFood (Food)
        // EXPORTS:     add (boolean)

        boolean add = false;

        // If inFood is null
        if (! validateObject(inFood))
        {
            throw new IllegalArgumentException("Invalid Fridge: Item is not valid, cannot check if can be added");
        }
        else
        {
            // Check if inFood can be stored
            add = (validateTemp(inFood));
        }

        return add;
    }

    // Setters
    public void setName(String inName)
    {
        // NAME:        setName
        // PURPOSE:     Change the name variable
        // ASSERTION:   name will be replaced by inName
        // IMPORTS:     inName (String)
        // EXPORTS:     none

        // If inName is null or empty
        if (! validateString(inName))
        {
            throw new IllegalArgumentException("Invalid Fridge: Name is either null or empty");
        }
        else
        {
            name = inName;
        }
    }

    @Override
    public void setItem(Food inItem)
    {
        // NAME:        setItem
        // PURPOSE:     Change the item variable
        // ASSERTION:   item will be replaced by inItem
        // IMPORTS:     inItem (Food)
        // EXPORTS:     none

        // If inItem is null
        if (! validateObject(inItem))
        {
            throw new IllegalArgumentException("Invalid Fridge: Item is not valid");
        }
        // If inItem's storage temp is out of range
        else if (! validateTemp(inItem))
        {
            throw new IllegalArgumentException("Invalid Fridge: Item cannot be stored, temperature is out of range");
        }
        else
        {
            item = inItem.clone();
            hasItem = true;
        }
    }

    // Getters
    @Override
    public double getLower() { return LOWERTEMP; }
    @Override
    public double getUpper() { return UPPERTEMP; }
    @Override
    public String getName() { return name; }
    @Override
    public Food getItem() { return item.clone(); }
    @Override
    public boolean getHasItem() { return hasItem; }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Compare inObj to this object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a Fridge
        if (inObj instanceof Fridge)
        {
            Fridge inFridge = (Fridge)inObj;
            isEquals = (
                (
                    realCompare(LOWERTEMP, inFridge.getLower())
                ) && (
                    realCompare(UPPERTEMP, inFridge.getUpper())
                ) && (
                    name.equals(inFridge.getName())
                ) && (
                    item.equals(inFridge.getItem())
                )
            );
        }

        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Assemble the contents of this object into a string
        // ASSERTION:   Return a string of all the variables in Fridge as well as item
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = "Storage type: " + name + "\n"
            + "Lower storage temp: " + LOWERTEMP + "\n"
            + "Upper storage temp: " + UPPERTEMP + "\n"
            + item.toString();
        return str;
    }

    @Override
    public Fridge clone()
    {
        // NAME:        clone
        // PURPOSE:     Clone this object
        // ASSERTION:   A clone will be made
        // IMPORTS:     none
        // EXPORTS:     cloneFridge (Fridge)

        Fridge cloneFridge = new Fridge(this);
        return cloneFridge;
    }

    @Override
    public Fridge placeholder()
    {
        // NAME:        placeholder
        // PURPOSE:     Create a placeholder Fridge
        // ASSERTION:   Placeholder Fridge object will be created
        // IMPORTS:     none
        // EXPORTS:     placeholder (Fridge)

        Fridge placeholder = new Fridge("Fridge");
        return placeholder;
    }

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     See if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean validateObject(Object inObj)
    {
        // NAME:        validateObject
        // PURPOSE:     Check if inObj is null
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (inObj != null);
        return isValid;
    }

    private boolean validateTemp(Food inItem)
    {
        // NAME:        validateTemp
        // PURPOSE:     Check if inItem's storage temp is within range
        // ASSERTION:   Return true or false
        // IMPORTS:     inItem (Food)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (
            (
                (inItem.getTempStorage() > LOWERTEMP) || (realCompare(inItem.getTempStorage(), LOWERTEMP))
            ) && (
                (inItem.getTempStorage() < UPPERTEMP) || (realCompare(inItem.getTempStorage(), UPPERTEMP))
            )
        );
        return isValid;
    }

    private boolean realCompare(double num1, double num2)
    {
        // NAME:        realCompare
        // PURPOSE:     Check if two real numbers are equals
        // ASSERTION:   Return true or false
        // IMPORTS:     num1 (double), num2 (double)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;
        isEquals = (Math.abs(num1 - num2) < 0.0000001);
        return isEquals;
    }
}
