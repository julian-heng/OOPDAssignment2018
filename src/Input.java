import java.util.*;

public class Input
{
    /**
     *  CLASSNAME:      Input
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Container class containing methods needed to get user input
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  PUBLIC METHODS:
     *      - integer (no boundary check)
     *      - integer (boundary check)
     *      - real (no boundary check)
     *      - real (boundary check)
     *      - string
     *      - character
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - realCompare
     **/

    public static int integer(String prompt, String errorMsg)
    {
        // NAME:        integer (no boundary check)
        // PURPOSE:     Prompt user and get integer input
        // ASSERTION:   Return integer user input
        // IMPORTS:     prompt (String), errorMsg (String)
        // EXPORTS:     userInput (int)

        Scanner sc = new Scanner(System.in);
        int userInput = 0;
        boolean inputValid = false;

        // If prompt is not empty
        if (validateString(prompt))
        {
            System.out.println(prompt);
        }

        // Loop until input is valid
        do
        {
            try
            {
                userInput = sc.nextInt();
                inputValid = true;
            }
            // Catch wrong type of input
            catch (InputMismatchException e)
            {
                // Clear userInput
                String flush = sc.nextLine();

                // Print error message
                System.out.println(errorMsg);
                inputValid = false;
            }
        } while (! inputValid);

        return userInput;
    }

    public static int integer(String prompt, String inErrorMsg, int lower, int upper)
    {
        // NAME:        integer (boundary check)
        // PURPOSE:     Get integer input within a range
        // ASSERTION:   Return integer user input
        // IMPORTS:     prompt (String), inErrorMsg (String), lower (int), upper (int)
        // EXPORTS:     userInput (int)
        
        Scanner sc = new Scanner(System.in);
        int userInput = 0;
        boolean inputValid = false;

        // If prompt is not empty
        if (validateString(prompt))
        {
            System.out.println(prompt);
        }

        // Loop until input is valid
        do
        {
            try
            {
                userInput = sc.nextInt();
                
                // Check if userInput is within range
                if (userInput < lower)
                {
                    String errorMsg;
                    errorMsg = "Error: Input is lower than " + lower + "\n"
                             + "Please try again";
                    System.out.println(errorMsg);
                    inputValid = false;
                }
                else if (userInput > upper)
                {
                    String errorMsg;
                    errorMsg = "Error: Input is higher than " + upper + "\n"
                             + "Please try again";
                    System.out.println(errorMsg);
                    inputValid = false;
                }
                else
                {
                    inputValid = true;
                }
            }
            catch (InputMismatchException e)
            {
                String flush = sc.nextLine();
                System.out.println(inErrorMsg);
                inputValid = false;
            }
        } while (! inputValid);

        return userInput;
    }

    public static double real(String prompt, String errorMsg)
    {
        // NAME:        real (no boundary check)
        // PURPOSE:     Prompt user and get real input
        // ASSERTION:   Return real user input
        // IMPORTS:     prompt (String), errorMsg (String)
        // EXPORTS:     userInput (double)
        
        Scanner sc = new Scanner(System.in);
        double userInput = 0.0;
        boolean inputValid = false;

        // If prompt is not empty
        if (validateString(prompt))
        {
            System.out.println(prompt);
        }

        // Loop until input is valid
        do
        {
            try
            {
                userInput = sc.nextDouble();
                inputValid = true;
            }
            catch (InputMismatchException e)
            {
                String flush = sc.nextLine();
                System.out.println(errorMsg);
                inputValid = false;
            }
        } while (! inputValid);

        return userInput;
    }

    public static double real(String prompt, String inErrorMsg, double lower, double upper)
    {
        // NAME:        real (boundary check)
        // PURPOSE:     Prompt user and get real input
        // ASSERTION:   Return integer real input
        // IMPORTS:     prompt (String), inErrorMsg (String), lower (double), upper (double)
        // EXPORTS:     userInput (double)
        
        Scanner sc = new Scanner(System.in);
        double userInput = 0.0;
        boolean inputValid = false;

        // If prompt is not empty
        if (validateString(prompt))
        {
            System.out.println(prompt);
        }

        // Loop until input is valid
        do
        {
            try
            {
                userInput = sc.nextDouble();

                // Check if userInput is within range
                if (! ((userInput > lower) || (realCompare(userInput, lower))))
                {
                    String errorMsg;
                    errorMsg = "Error: Input is lower than " + lower + "\n"
                             + "Please try again";
                    System.out.println(errorMsg);
                    inputValid = false;
                }
                else if (! ((userInput < upper) || (realCompare(userInput, upper))))
                {
                    String errorMsg;
                    errorMsg = "Error: Input is higher than " + upper + "\n"
                             + "Please try again";
                    System.out.println(errorMsg);
                    inputValid = false;
                }
                else
                {
                    inputValid = true;
                }
            }
            catch (InputMismatchException e)
            {
                String flush = sc.nextLine();
                System.out.println(inErrorMsg);
                inputValid = false;
            }
        } while (! inputValid);

        return userInput;
    }

    public static String string(String prompt, String errorMsg)
    {
        // NAME:        string
        // PURPOSE:     Prompt user and get string input
        // ASSERTION:   Return string user input
        // IMPORTS:     prompt (String), errorMsg (String)
        // EXPORTS:     userInput (String)
        
        Scanner sc = new Scanner(System.in);
        String userInput;
        boolean inputValid = false;

        // If prompt is not empty
        if (validateString(prompt))
        {
            System.out.println(prompt);
        }

        // Loop until input is valid
        do
        {
            userInput = sc.nextLine();
            if (validateString(userInput))
            {
                inputValid = true;
            }
        } while (! inputValid);

        return userInput;
    }

    public static char character(String prompt, String errorMsg)
    {
        // NAME:        character
        // PURPOSE:     Prompt user and get character input
        // ASSERTION:   Return character user input
        // IMPORTS:     prompt (String), errorMsg (String)
        // EXPORTS:     userInput (char)
        
        Scanner sc = new Scanner(System.in);
        char userInput = 'a';
        boolean inputValid = false;

        // If prompt is not empty
        if (validateString(prompt))
        {
            System.out.println(prompt);
        }

        // Loop until input is valid
        do
        {
            try
            {
                userInput = sc.next().charAt(0);
                inputValid = true;
            }
            catch (InputMismatchException e)
            {
                String flush = sc.nextLine();
                System.out.println(errorMsg);
                inputValid = false;
            }
        } while (! inputValid);

        return userInput;
    }

    private static boolean validateString(String str)
    {
        // NAME:        validateString
        // ASSERTION:   Return true or false
        // PURPOSE:     Check to see if string is not null and not empty
        // IMPORTS:     str (String)
        // EXPORTS:     isValid (boolean)
        
        boolean isValid = false;
        isValid = ((str != null) && (! str.isEmpty()));
        return isValid;
    }

    private static boolean realCompare(double num1, double num2)
    {
        // NAME:        realComapre
        // PURPOSE:     Check to see two real numbers are the same
        // ASSERTION:   Return true or false
        // IMPORTS:     num1 (double), num2 (double)
        // EXPORTS:     isValid (boolean)
        
        boolean isEquals = false;
        isEquals = (Math.abs(num1 - num2) < 0.0000001);
        return isEquals;
    }
}
