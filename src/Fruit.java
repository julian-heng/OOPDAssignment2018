import java.util.*;

public class Fruit extends Food implements IFood
{
    /**
     *  CLASSNAME:      Fruit
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Create a fruit object containing characteristics of the food item
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASSFIELDS:
     *      - type (String)
     *      - numPieces (int)
     *      - useBy (Date)
     *      - expired (boolean)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor
     *      - Copy Constructor
     *      - calcExpire
     *      - toCSV
     *      - setType
     *      - setNumPieces
     *      - setUseBy
     *      - getType
     *      - getNumPieces
     *      - getUseBy
     *      - getExpired
     *      - getItemTypeStr
     *      - getItemType
     *      - clone
     *      - equals
     *      - toString
     *
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateInt
     *      - validateObject
     *      - validateDate
     **/

    // Declare class fields
    private String type;
    private int numPieces;
    private Date useBy;
    private boolean expired;

    // Constructors
    public Fruit()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a fruit object with placeholder values
        // ASSERTION:   Default fruit object will be created
        // IMPORTS:     none
        // EXPORTS:     none

        // Call super class
        super();
        
        // Set default values
        type = "Default type";
        numPieces = 10;
        useBy = new Date();
        expired = validateDate(useBy);
    }

    public Fruit(String inName, double inTempStorage, String inPackageType, String inType, int inNumPieces, Date inUseBy)
    {
        // NAME:        Alternate Constructor
        // PURPOSE:     Create a fruit object with imports
        // ASSERTION:   Fruit object with imports will be created
        // IMPORTS:     inName (String), intempStorage (double), inPackageType (String), inType (String), inNumPieces (double), inUseBy (Date)
        // EXPORTS:     none

        // Call super class
        super(inName, inTempStorage, inPackageType);

        // Call setters
        setType(inType);
        setNumPieces(inNumPieces);
        setUseBy(inUseBy);
    }

    public Fruit(Fruit inFruit)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Copy the contents of the fruit import into a new object
        // ASSERTION:   Copy inFruit object to new fruit object
        // IMPORTS:     inFruit (Fruit)
        // EXPORTS:     none

        // Call super class
        super(inFruit);

        // Call getters in inFruit
        type = inFruit.getType();
        numPieces = inFruit.getNumPieces();
        useBy = inFruit.getUseBy();
    }

    // Mutators
    @Override
    public boolean calcExpire(Date today)
    {
        // NAME:        calcExpire
        // PURPOSE:     Determine if the object expire date has passed current date
        // ASSERTION:   Return true or false
        // IMPORTS:     today (Date)
        // EXPORTS:     isExpire (boolean)

        boolean isExpire = false;
        isExpire = today.isBehind(useBy);
        return isExpire;
    }

    @Override
    public String toCSV()
    {
        // NAME:        toCSV
        // PURPOSE:     Combine the contents of this object into a csv line
        // ASSERTION:   Return csv line from this object
        // IMPORTS:     none
        // EXPORTS:     csvLine (String)

        String csvLine;

        // Assemble csv line
        csvLine = getItemTypeStr() + "," 
                + super.getName() + "," 
                + numPieces + "," 
                + super.getTempStorage() + "," 
                + useBy.toString() + "," 
                + super.getPackageType();
        return csvLine;
    }

    // Setters
    public void setType(String inType)
    {
        // NAME:        setType
        // PURPOSE:     Change the type variable to inType
        // ASSERTION:   type will be replaced by inType
        // IMPORTS:     inType (String)
        // EXPORTS:     none

        // If inType is either null or empty
        if (! validateString(inType))
        {
            throw new IllegalArgumentException("Invalid Fruit: Cannot set type, is either null or empty");
        }
        else
        {
            type = inType;
        }
    }

    public void setNumPieces(int inNumPieces)
    {
        // NAME:        setNumPieces
        // PURPOSE:     Change the numPieces variable to inNumPieces
        // ASSERTION:   numPieces will be replaced by inNumPieces
        // IMPORTS:     inNumPieces (double)
        // EXPORTS:     none

        // If inNumPieces is not in between 1 and 20 inclusive
        if (! validateInt(inNumPieces))
        {
            throw new IllegalArgumentException("Invalid Fruit: Cannot set numPieces, is less than 0");
        }
        else
        {
            numPieces = inNumPieces;
        }
    }

    public void setUseBy(Date inUseBy)
    {
        // NAME:        setUseBy
        // PURPOSE:     Change the useBy date object to inUseBy
        // ASSERTION:   useBy will be replaced by inUseBy
        // IMPORTS:     inUseBy (Date)
        // EXPORTS:     none

        // If either inUseBy is null or date is in the past
        if ((! validateObject(inUseBy)) || (validateDate(inUseBy)))
        {
            throw new IllegalArgumentException("Invalid Fruit: Cannot set expire by date, not valid");
        }
        else
        {
            useBy = new Date(inUseBy);
            expired = validateDate(useBy);
        }
    }

    // Getters
    public String getType() { return type; }
    public int getNumPieces() { return numPieces; }
    public Date getUseBy() { return new Date(useBy); }
    public boolean getExpired() { return expired; }

    @Override
    public String getItemTypeStr() { return "Fruit"; }

    @Override
    public char getItemType() { return 'F'; }

    @Override
    public Fruit clone()
    {
        // NAME:        clone
        // PURPOSE:     Clone this object
        // ASSERTION:   A clone will be made
        // IMPORTS:     none
        // EXPORTS:     cloneFruit (Fruit)

        Fruit cloneFruit = new Fruit(this);
        return cloneFruit;
    }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Compare inObj to current object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a fruit object
        if (inObj instanceof Fruit)
        {
            Fruit inFruit = (Fruit)inObj;
            isEquals = (
                (
                    super.equals(inFruit)
                ) && (
                    type.equals(inFruit.getType())
                ) && (
                    numPieces == inFruit.getNumPieces()
                ) && (
                    useBy.equals(inFruit.getUseBy())
                ) && (
                    expired == inFruit.getExpired()
                )
            );
        }
        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Convert all the contents of fruit object to a string
        // ASSERTION:   Return a string with all characteristics to this object
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = "Item type: " + getItemTypeStr() + "\n"
            + super.toString() + "\n"
            + "Type: " + type + "\n"
            + "No. of Pieces: " + numPieces + "\n"
            + "Use by: " + useBy.toString() + "\n"
            + "Expired: " + expired;
        return str;
    }

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     none

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean validateInt(int inInt)
    {
        // NAME:        validateInt
        // PURPOSE:     Check if inInt is more than 1 but less than 20 inclusive
        // ASSERTION:   Return true or false
        // IMPORTS:     inInt (double)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = ((inInt > 0) && (inInt < 21));
        return isValid;
    }

    private boolean validateObject(Object inObject)
    {
        // NAME:        validateObject
        // PURPOSE:     Check if inObject is null
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (inObject != null);
        return isValid;
    }

    private boolean validateDate(Date inUseBy)
    {
        // NAME:        validateDate
        // PURPOSE:     Check if date is not in the past
        // ASSERTION:   Return true or false
        // IMPORTS:     inUseBy (Date)
        // EXPORTS:     isValid (boolean)

        Date today = new Date().setToday();
        boolean isValid = false;
        isValid = today.isBehind(inUseBy);
        return isValid;
    }
}
