import java.util.*;

public interface IFood
{
    /**
     *  INTERFACE NAME: IFood
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Interface for food objects
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  ABSTRACT METHODS:
     *      - calcExpire
     **/

    public boolean calcExpire(Date today);
    //public int calcSpace(Food food);
}
