import java.util.*;

public class Date
{
    /**
     *  CLASSNAME:      Date
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Create a date object to store date information
     *  FIRST CREATED:  22/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASSFIELDS:
     *      - day (int)
     *      - month (int)
     *      - year (int)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor (From integer)
     *      - Alternate Constructor (From string)
     *      - Copy Constructor
     *      - setToday
     *      - isAhead
     *      - isBehind
     *      - setDate (From integer)
     *      - setDate (From string)
     *      - getDay
     *      - getMonth
     *      - getYear
     *      - clone
     *      - equals
     *      - toString
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateDate
     *      - checkRange
     *      - checkLeapYear
     *      - addPadding
     **/

    // Declare class fields
    private int day, month, year;

    public Date()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a default date object set to 01/01/1970
        // ASSERTION:   Default date object will be created
        // IMPORTS:     none
        // EXPORTS:     none

        day = 1;
        month = 1;
        year = 1970;
    }

    public Date(int inDay, int inMonth, int inYear)
    {
        // NAME:        Alternate Constructor (From integer)
        // PURPOSE:     Create a date object with integer imports
        // ASSERTION:   Date object with integer imports will be created
        // IMPORTS:     inDay (int), inMonth (int), inYear (int)
        // EXPORTS:     none

        // Call the setter to set the values to day, month and year
        setDate(inDay, inMonth, inYear);
    }

    public Date(String inDate)
    {
        // NAME:        Alternate Constructor (From string)
        // PURPOSE:     Create a date object with string import
        // ASSERTION:   Date object with string import will be created
        // IMPORTS:     inDate (String)
        // EXPORTS:     none

        // Call the setter to set the values
        setDate(inDate);
    }

    public Date(Date inDate)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Copy the contents of the date import into a new object
        // ASSERTION:   Copy inDate object to new date object
        // IMPORTS:     inDate (Date)
        // EXPORTS:     none

        // Call getters in inDate
        day = inDate.getDay();
        month = inDate.getMonth();
        year = inDate.getYear();
    }

    public Date setToday()
    {
        // NAME:        setToday
        // PURPOSE:     Set the day, month and year variables to current date
        // ASSERTION:   Date object will contain current date
        // IMPORTS:     none
        // EXPORTS:     today (Date)

        Date today;
        int nowDay, nowMonth, nowYear;
        Calendar cal = Calendar.getInstance();

        // Get current date from Calendar
        nowDay = cal.get(Calendar.DATE);
        nowMonth = cal.get(Calendar.MONTH) + 1;
        nowYear = cal.get(Calendar.YEAR);

        // Create Date object with current date
        today = new Date(nowDay, nowMonth, nowYear);

        return today;
    }

    public boolean isAhead(Date inDate)
    {
        // NAME:        isAhead
        // PURPOSE:     Check if inDate is ahead of this date
        // ASSERTION:   Return true or false
        // IMPORTS:     inDate (Date)
        // EXPORTS:     isAhead (boolean)

        boolean isAhead = false;

        // Check if the dates are not the same
        if (! equals(inDate))
        {
            // Check if
            //     - inDate year is more than year, or
            //     - inDate year is the same, with inDate month is more than month, or
            //     - inDate year and month is the same, with inDate day is more than day
            if (
                (
                    (inDate.getYear() > year)
                ) || (
                    (inDate.getYear() == year) && (inDate.getMonth() > month)
                ) || (
                    (inDate.getYear() == year) && (inDate.getMonth() == month) && (inDate.getDay() > day)
                )
            )
            {
                isAhead = true;
            }
        }
        return isAhead;
    }

    public boolean isBehind(Date inDate)
    {
        // NAME:        isBehind
        // PURPOSE:     Check if inDate is behind of this date
        // ASSERTION:   Return true or false
        // IMPORTS:     inDate (Date)
        // EXPORTS:     isBehind (boolean)

        boolean isBehind = false;

        // Check if the dates are not the same
        if (! equals(inDate))
        {
            // Check if
            //     - inDate year is less than year, or
            //     - inDate year is the same, with inDate month is less than month, or
            //     - inDate year and month is the same, with inDate day is less than day
            if (
                (
                    (inDate.getYear() < year)
                ) || (
                    (inDate.getYear() == year) && (inDate.getMonth() < month)
                ) || (
                    (inDate.getYear() == year) && (inDate.getMonth() == month) && (inDate.getDay() < day)
                )
            )
            {
                isBehind = true;
            }
        }
        return isBehind;
    }

    public void setDate(int inDay, int inMonth, int inYear)
    {
        // NAME:        setDate (from integer)
        // PURPOSE:     Set the date using integer imports
        // ASSERTION:   day, month and year variables will be set to imports
        // IMPORTS:     inDay (int), inMonth (int), inYear (int)
        // EXPORTS:     none

        // If the imports are not valid
        if (! validateDate(inDay, inMonth, inYear))
        {
            throw new IllegalArgumentException("Invalid Date: Date is not valid");
        }
        else
        {
            // Change the variables
            day = inDay;
            month = inMonth;
            year = inYear;
        }
    }

    public void setDate(String inDate)
    {
        // NAME:        setDate (from string)
        // PURPOSE:     Set the date using string imports
        // ASSERTION:   day, month and year variables will be set to imports
        // IMPORTS:     inDate (String)
        // EXPORTS:     none

        int inDay, inMonth, inYear;
        String[] splitDate;

        // If inDate is null or empty
        if (! validateString(inDate))
        {
            throw new IllegalArgumentException("Invalid Date: Date is either null or empty");
        }
        else
        {
            // Split inDate using forward slash
            splitDate = inDate.split("/");

            // If inDate format is not valid
            if (splitDate.length != 3)
            {
                throw new IllegalArgumentException("Invalid Date: Date is not a valid format");
            }
            else
            {
                // Assign the splitted line to integers
                inDay = Integer.parseInt(splitDate[0]);
                inMonth = Integer.parseInt(splitDate[1]);
                inYear = Integer.parseInt(splitDate[2]);

                // Call the setters to set day, month and year
                setDate(inDay, inMonth, inYear);
            }
        }
    }

    // Getters
    public int getDay() { return day; }
    public int getMonth() { return month; }
    public int getYear() { return year; }

    public Date clone()
    {
        // NAME:        clone
        // PURPOSE:     Clone this object
        // ASSERTION:   A clone will be made
        // IMPORTS:     none
        // EXPORTS:     clone (Date)

        Date clone = new Date(this);
        return clone;
    }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Compare inObj to this object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a Date object
        if (inObj instanceof Date)
        {
            Date inDate = (Date)inObj;
            isEquals = (
                (
                    day == inDate.getDay()
                ) && (
                    month == inDate.getMonth()
                ) && (
                    year == inDate.getYear()
                )
            );
        }

        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Export the contents of this object as a string
        // ASSERTION:   Return a string formated as dd/mm/yyyy
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = addPadding(day) + "/" + addPadding(month) + "/" + year;
        return str;
    }

    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean validateDate(int inDay, int inMonth, int inYear)
    {
        // NAME:        validateDate
        // PURPOSE:     Check if date is valid
        // ASSERTION:   Return true or false
        // IMPORTS:     inDay (int), inMonth (int), inYear (int)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;

        // Check if inDay, inMonth and inYear is not 0
        if ((inDay != 0) && (inMonth != 0) && (inYear != 0))
        {
            // Determine which month in order to determine maximum number of days
            switch (inMonth)
            {
                case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                    isValid = (checkRange(inDay, 1, 31));
                    break;
                case 4: case 6: case 9: case 11:
                    isValid = (checkRange(inDay, 1, 30));
                    break;
                case 2:
                    if (checkLeapYear(inYear))
                    {
                        isValid = (checkRange(inDay, 1, 29));
                    }
                    else
                    {
                        isValid = (checkRange(inDay, 1, 28));
                    }
                    break;
            }
        }

        return isValid;
    }

    private boolean checkRange(int inNum, int lower, int upper)
    {
        // NAME:        checkRange
        // PURPOSE:     See if inNum falls in between lower and upper
        // ASSERTION:   Return true or false
        // IMPORTS:     inNum (int), lower (int), upper (int)
        // EXPORTS:     inRange (boolean)

        boolean inRange = false;
        inRange = ((inNum >= lower) && (inNum <= upper));
        return inRange;
    }

    private boolean checkLeapYear(int inYear)
    {
        // NAME:        checkLeapYear
        // PURPOSE:     Check if inYear is a leap year
        // ASSERTION:   Return true or false
        // IMPORTS:     inYear (int)
        // EXPORTS:     isLeapYear (boolean)

        boolean isLeapYear = false;

        // Formula for calculating leap year
        // Source: https://en.wikipedia.org/wiki/Leap_year#Algorithm
        isLeapYear = ((((inYear % 4) == 0) && ((inYear % 100) != 0)) || ((inYear % 400) == 0));
        return isLeapYear;
    }

    private String addPadding(int inNum)
    {
        // NAME:        addPadding
        // PURPOSE:     Ensure that when inNum is converted to string, it is at least 2 characters long
        // ASSERTION:   Return modified string convertion of inNum
        // IMPORTS:     inNum (int)
        // EXPORTS:     str (String)

        String str;
        if (checkRange(inNum, 1, 9))
        {
            str = "0" + inNum;
        }
        else
        {
            str = Integer.toString(inNum);
        }

        return str;
    }
}
