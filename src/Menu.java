import java.util.*;

public class Menu
{
    /**
     *  CLASSNAME:      Menu
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Create a menu object to store options and return user input
     *  FIRST CREATED:  10/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  PUBLIC METHODS:
     *      - getUserInput
     *
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateOptions
     *      - printMenu
     **/

    public static int getUserInput(String[] inOptions)
    {
        // NAME:        getUserInput
        // PURPOSE:     A wrapper method to print options and get user input
        // ASSERTION:   The choice made by the user will be returned
        // IMPORTS:     inOption (String array)
        // EXPORTS:     userInput (int)

        int userInput;
        String errorMsg;

        // If inOptions is not valid
        if (! validateOptions(inOptions))
        {
            throw new IllegalArgumentException("Invalid Options: Options are not valid");
        }
        else
        {
            // Print menu
            printMenu(inOptions);
            errorMsg = "Error: Input must be an integer\n"
                     + "Please try again";

            // Get user input
            userInput = Input.integer("", errorMsg, 1, inOptions.length - 1);
        }

        return userInput;
    }

    private static boolean validateOptions(String[] inOptions)
    {
        // NAME:        validateOptions
        // PURPOSE:     Check to see if existing options is not null and not empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inOptions (String array)
        // EXPORTS:     valid (boolean)

        boolean valid = true;
        int i;
        i = 0;
        
        // While valid is still true and not exceeding inOptions
        while ((valid) && (i < inOptions.length))
        {
            // Validate the string in index in inOption
            if (! validateString(inOptions[i]))
            {
                // If not valid, then set valid to false
                valid = false;
            }
            i += 1;
        }

        return valid;
    }

    private static boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check to see if string is not null and not empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     valid (boolean)

        boolean valid = ((inString != null) && (! inString.isEmpty()));
        return valid;
    }

    private static void printMenu(String[] inOptions)
    {
        // NAME:        printMenu
        // PURPOSE:     Print the menu
        // ASSERTION:   Output the menu to display
        // IMPORTS:     inOptions (String array)
        // EXPORTS:     none

        // Loop through options and print each out
        for (int i = 0; i < inOptions.length; i++)
        {
            if (validateString(inOptions[i]))
            {
                System.out.println(inOptions[i]);
            }
        }

        // Print newline
        System.out.println();
    }
}
