import java.util.*;

public class Meat extends Food implements IFood
{
    /**
     *  CLASSNAME:      Meat
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Create a meat object containing characteristics of the food item
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASSFIELDS:
     *      - cut (String)
     *      - weight (double)
     *      - useBy (Date)
     *      - expired (boolean)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor
     *      - Copy Constructor
     *      - calcExpire
     *      - toCSV
     *      - setCut
     *      - setWeight
     *      - setUseBy
     *      - getCut
     *      - getWeight
     *      - getUseBy
     *      - getExpired
     *      - getItemTypeStr
     *      - getItemType
     *      - clone
     *      - equals
     *      - toString
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateReal
     *      - validateObject
     *      - validateDate
     *      - realCompare
     **/

    // Declare class fields
    private String cut;
    private double weight;
    private Date useBy;
    private boolean expired;

    // Constructors
    public Meat()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a meat object with placeholder values
        // ASSERTION:   Default meat object will be created
        // IMPORTS:     none
        // EXPORTS:     none

        // Call super class
        super();

        // Set default values
        cut = "Default cut";
        weight = 10.0;
        useBy = new Date();
        expired = validateDate(useBy);
    }

    public Meat(String inName, double inTempStorage, String inPackageType, String inCut, double inWeight, Date inUseBy)
    {
        // NAME:        Alternate Constructor
        // PURPOSE:     Create a meat object with imports
        // ASSERTION:   Meat object with imports will be created
        // IMPORTS:     inName (String), intempStorage (double), inPackageType (String), inCut (String), inWeight (double), inUseBy (Date)
        // EXPORTS:     none

        // Call super class
        super(inName, inTempStorage, inPackageType);

        // Call setters
        setCut(inCut);
        setWeight(inWeight);
        setUseBy(inUseBy);
    }

    public Meat(Meat inMeat)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Copy the contents of the meat import into a new object
        // ASSERTION:   Copy inMeat object to new meat object
        // IMPORTS:     inMeat (Meat)
        // EXPORTS:     none

        // Call super class
        super(inMeat);

        // Call getters in inMeat
        cut = inMeat.getCut();
        weight = inMeat.getWeight();
        useBy = inMeat.getUseBy();
        expired = inMeat.getExpired();
    }

    // Mutators
    @Override
    public boolean calcExpire(Date today)
    {
        // NAME:        calcExpire
        // PURPOSE:     Determine if the object expire date has passed current date
        // ASSERTION:   Return true or false
        // IMPORTS:     today (Date)
        // EXPORTS:     isExpire (boolean)

        boolean isExpire = false;
        isExpire = today.isBehind(useBy);
        return isExpire;
    }

    @Override
    public String toCSV()
    {
        // NAME:        toCSV
        // PURPOSE:     Combine the contents of this object into a csv line
        // ASSERTION:   Return csv line from this object
        // IMPORTS:     none
        // EXPORTS:     csvLine (String)

        String csvLine;

        // Assemble csv line
        csvLine = getItemTypeStr() + "," 
                + super.getName() + "," 
                + cut + "," 
                + weight + "," 
                + super.getTempStorage() + ","
                + useBy.toString() + "," 
                + super.getPackageType();
        return csvLine;
    }

    // Setters
    public void setCut(String inCut)
    {
        // NAME:        setCut
        // PURPOSE:     Change the cut variable to inCut
        // ASSERTION:   cut will be replaced by inCut
        // IMPORTS:     inCut (String)
        // EXPORTS:     none

        // If inCut is either null or empty
        if (! validateString(inCut))
        {
            throw new IllegalArgumentException("Invalid Meat: Cannot set cut, is either null or empty");
        }
        else
        {
            cut = inCut;
        }
    }

    public void setWeight(double inWeight)
    {
        // NAME:        setWeight
        // PURPOSE:     Change the weight variable to inWeight
        // ASSERTION:   weight will be replaced by inWeight
        // IMPORTS:     inWeight (double)
        // EXPORTS:     none

        // If inWeight is not in between 0.2 and 5.0
        if (! validateReal(inWeight))
        {
            throw new IllegalArgumentException("Invalid Meat: Cannot set weight, it is not in between 0.2 and 5.0");
        }
        else
        {
            weight = inWeight;
        }
    }

    public void setUseBy(Date inUseBy)
    {
        // NAME:        setUseBy
        // PURPOSE:     Change the useBy date object to inUseBy
        // ASSERTION:   useBy will be replaced by inUseBy
        // IMPORTS:     inUseBy (Date)
        // EXPORTS:     none

        // If either inUseBy is null or date is in the past
        if ((! validateObject(inUseBy)) || (validateDate(inUseBy)))
        {
            throw new IllegalArgumentException("Invalid Meat: Cannot set use by date, not valid");
        }
        else
        {
            useBy = new Date(inUseBy);
            expired = validateDate(useBy);
        }
    }

    // Getters
    public String getCut() { return cut; }
    public double getWeight() { return weight; }
    public Date getUseBy() { return new Date(useBy); }
    public boolean getExpired() { return expired; }

    @Override
    public String getItemTypeStr() { return "Meat"; }

    @Override
    public char getItemType() { return 'M'; }

    @Override
    public Meat clone()
    {
        // NAME:        clone
        // PURPOSE:     Clone this object
        // ASSERTION:   A clone will be made
        // IMPORTS:     none
        // EXPORTS:     cloneMeat (Meat)

        Meat cloneMeat = new Meat(this);
        return cloneMeat;
    }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Compare inObj to current object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a meat object
        if (inObj instanceof Meat)
        {
            Meat inMeat = (Meat)inObj;
            isEquals = (
                (
                    super.equals(inMeat)
                ) && (
                    cut.equals(inMeat.getCut())
                ) && (
                    realCompare(weight, inMeat.getWeight())
                ) && (
                    useBy.equals(inMeat.getUseBy())
                ) && (
                    expired == inMeat.getExpired()
                )
            );
        }

        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Convert all the contents of meat object to a string
        // ASSERTION:   Return a string with all characteristics to this object
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = "Item type: " + getItemTypeStr() + "\n"
            + super.toString() + "\n"
            + "Cut: " + cut + "\n"
            + "Weight: " + weight + "\n"
            + "Use by: " + useBy.toString() + "\n"
            + "Expired: " + expired;
        return str;
    }

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     none

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean validateReal(double inReal)
    {
        // NAME:        validateReal
        // PURPOSE:     Check if inReal is more than 0.2 but less than 5.0
        // ASSERTION:   Return true or false
        // IMPORTS:     inReal (double)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (
            (
                (inReal > 0.2) || (realCompare(inReal, 0.2))
            ) && (
                (inReal < 5.0) || (realCompare(inReal, 5.0))
            )
        );
        return isValid;
    }

    private boolean validateObject(Object inObject)
    {
        // NAME:        validateObject
        // PURPOSE:     Check if inObject is null
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (inObject != null);
        return isValid;
    }

    private boolean validateDate(Date inUseBy)
    {
        // NAME:        validateDate
        // PURPOSE:     Check if date is not in the past
        // ASSERTION:   Return true or false
        // IMPORTS:     inUseBy (Date)
        // EXPORTS:     isValid (boolean)

        Date today = new Date().setToday();
        boolean isValid = false;
        isValid = today.isBehind(inUseBy);
        return isValid;
    }

    private boolean realCompare(double num1, double num2)
    {
        // NAME:        realCompare
        // PURPOSE:     Check if two real numbers are the same value
        // ASSERTION:   Return true or false
        // IMPORTS:     num1 (double), num2 (double)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;
        isEquals = (Math.abs(num1 - num2) < 0.0000001);
        return isEquals;
    }
}
