import java.util.*;

public abstract class Storage
{
    /**
     *  CLASSNAME:      Storage
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Abstract class to Freezer, Fridge and Pantry
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  PUBLIC METHODS:
     *      - isExpired (Abstract)
     *      - toCSV (Abstract)
     *      - canAdd (Abstract)
     *      - setItem (Abstract)
     *      - getLower (Abstract)
     *      - getUpper (Abstract)
     *      - getName (Abstract)
     *      - getItem (Abstract)
     *      - getHasItem (Abstract)
     *      - clone (Abstract)
     *      - placeholder (Abstract)
     **/

    // Abstract methohds for mutators
    public abstract boolean isExpired();
    public abstract String toCSV();
    public abstract boolean canAdd(Food inFood);

    // Abstract methods for setters
    public abstract void setItem(Food inItem);

    // Abstract methods for getters
    public abstract double getLower();
    public abstract double getUpper();
    public abstract String getName();
    public abstract Food getItem();
    public abstract boolean getHasItem();

    public abstract Storage clone();
    public abstract Storage placeholder();
}
