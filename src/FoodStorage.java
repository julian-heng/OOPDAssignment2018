import java.util.*;

public class FoodStorage
{
    /**
     *  CLASSNAME:      FoodStorage
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Container class for both Food and Storage
     *  FIRST CREATED:  23/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASSFIELDS:
     *      - numFreezer (int)
     *      - numFridge (int)
     *      - numPantry (int)
     *      - numStorage (int)
     *      - numFreezerUsed (int)
     *      - numFridgeUsed (int)
     *      - numPantryUsed (int)
     *      - numTotalUsed (int)
     *      - numMeat (int)
     *      - numGrain (int)
     *      - numFruit (int)
     *      - numVegetable (int)
     *      - numFood (int)
     *      - food (Food array)
     *      - storage (Storage 2D array)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor (Storage and Food)
     *      - Alternate Constructor (Storage only)
     *      - Copy Constructor
     *      - addItem
     *      - removeItem
     *      - exportFoodStorage
     *      - findExpired
     *      - getConstructFromFile
     *      - getNumFreezer
     *      - getNumFridge
     *      - getNumPantry
     *      - getNumStorage
     *      - getNumFreezerUsed
     *      - getNumFridgeUsed
     *      - getNumPantryUsed
     *      - getNumTotalUsed
     *      - getNumMeat
     *      - getNumGrain
     *      - getNumFruit
     *      - getNumVegetable
     *      - getNumFood
     *      - getStorage
     *      - getFood
     *      - equals
     *      - displayStorage
     *      - toString
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - compareStorage
     *      - compareFood
     *      - deleteElement
     *      - findStorage
     *      - findStorageIndex
     *      - updateInventoryCount
     *      - countFood
     *      - calcFull
     **/

    // Declare class fields
    private Food[] food;
    private Storage[][] storage;
    private int numFreezer, numFridge, numPantry, numStorage;
    private int numFreezerUsed, numFridgeUsed, numPantryUsed, numTotalUsed;
    private int numMeat, numGrain, numFruit, numVegetable, numFood;
    private boolean isConstructFromFile = false;

    // Mutators
    public FoodStorage()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a default FoodStorage object
        // ASSERTION:   All class fields are set to 0 or default
        // IMPORTS:     none
        // EXPORTS:     none

        isConstructFromFile = false;

        storage = new Storage[3][];
        food = new Food[0];
    }

    public FoodStorage(Storage[][] inStorage, Food[] inFood)
    {
        // NAME:        Alternate Constructor (Storage and Food)
        // PURPOSE:     Create a FoodStorage object with both storage and food imports
        // ASSERTION:   Set storage and food and update class fields
        // IMPORTS:     inStorage (Storage 2D array), inFood (Food array)
        // EXPORTS:     none

        // If storage has already been made from file
        if (isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Storage: Already read from file");
        }
        else
        {
            // Set flag so that cannot add from file again
            isConstructFromFile = true;

            // Set object's storage array to the same size as inStorage
            storage = new Storage[inStorage.length][];

            // For each storage facility
            for (int i = 0; i < inStorage.length; i++)
            {
                // Check which kind of storage facility it is and create
                // the same size of array and same kind
                if (inStorage[i][0] instanceof Freezer)
                {
                    storage[i] = new Freezer[inStorage[i].length];
                }
                else if (inStorage[i][0] instanceof Fridge)
                {
                    storage[i] = new Fridge[inStorage[i].length];
                }
                else if (inStorage[i][0] instanceof Pantry)
                {
                    storage[i] = new Pantry[inStorage[i].length];
                }

                // For each slot in storage facility
                for (int j = 0; j < inStorage[i].length; j++)
                {
                    // Clone storage slot
                    storage[i][j] = inStorage[i][j].clone();
                }
            }
        }

        // For each element in inFood array
        for (int i = 0; i < inFood.length; i++)
        {
            // Clone to object food's array
            food[i] = inFood[i].clone();

            // Add item
            addItem(food[i]);
        }

        // Update class fields
        updateInventoryCount();
    }

    public FoodStorage(Storage[][] inStorage)
    {
        // NAME:        Alternate Constructor (Storage)
        // PURPOSE:     Create a FoodStorage object with storage
        // ASSERTION:   Set storage and update class fields
        // IMPORTS:     inStorage (Storage 2D array)
        // EXPORTS:     none

        // If Storage has already been made from file
        if (isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Storage: Already read from file");
        }
        else
        {
            // Same logic as above
            isConstructFromFile = true;
            storage = new Storage[inStorage.length][];

            for (int i = 0; i < inStorage.length; i++)
            {
                if (inStorage[i][0] instanceof Freezer)
                {
                    storage[i] = new Freezer[inStorage[i].length];
                }
                else if (inStorage[i][0] instanceof Fridge)
                {
                    storage[i] = new Fridge[inStorage[i].length];
                }
                else if (inStorage[i][0] instanceof Pantry)
                {
                    storage[i] = new Pantry[inStorage[i].length];
                }

                for (int j = 0; j < inStorage[i].length; j++)
                {
                    storage[i][j] = inStorage[i][j].clone();
                }
            }

            updateInventoryCount();
        }
    }

    public FoodStorage(FoodStorage inFoodStorage)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Create a copy of import inFoodStorage
        // ASSERTION:   Copy object will be created
        // IMPORTS:     inFoodStorage (FoodStorage)
        // EXPORTS:     none

        // Call getters in inFoodStorage
        isConstructFromFile = inFoodStorage.getConstructFromFile();
        numFreezer = inFoodStorage.getNumFreezer();
        numFridge = inFoodStorage.getNumFridge();
        numPantry = inFoodStorage.getNumPantry();
        numStorage = inFoodStorage.getNumStorage();
        
        numFreezerUsed = inFoodStorage.getNumFreezerUsed();
        numFridgeUsed = inFoodStorage.getNumFridgeUsed();
        numPantryUsed = inFoodStorage.getNumPantryUsed();
        numTotalUsed = inFoodStorage.getNumTotalUsed();

        numMeat = inFoodStorage.getNumMeat();
        numGrain = inFoodStorage.getNumGrain();
        numFruit = inFoodStorage.getNumFruit();
        numVegetable = inFoodStorage.getNumVegetable();
        numFood = inFoodStorage.getNumFood();

        storage = inFoodStorage.getStorage();
        food = inFoodStorage.getFood();
    }

    // Mutators
    public void addItem(Food inItem)
    {
        // NAME:        addItem
        // PURPOSE:     Add an item to the 2d storage array
        // ASSERTION:   Add item to storage array and update class fields
        // IMPORTS:     inItem (Food)
        // EXPORTS:     none

        int i, index;
        boolean added = false;

        i = 0;
        index = 0;

        // If have not constructed storage from file
        if (! isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Storage: Cannot add food until read from file");
        }
        else
        {
            // Update class fields
            updateInventoryCount();

            // If there is not more space in storage
            if (numTotalUsed == numStorage)
            {
                throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot add food, storage is full");
            }
            else
            {
                // added is false and i is less than length of storage
                while ((! added) && (i < storage.length))
                {
                    try
                    {
                        // If the item can be added into the storage facility
                        if (storage[i][0].canAdd(inItem))
                        {
                            // Get the next empty spot in storage
                            index = calcFull(storage[i]);

                            // If that storage facility is full
                            if (index >= storage[i].length)
                            {
                                throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot add food, not enough storage");
                            }
                            else
                            {
                                // Set that slot to the item
                                storage[i][index].setItem(inItem);
                                added = true;
                            }
                        }
                    }
                    // If catches an error, like cannot add to storage
                    catch (IllegalArgumentException e)
                    {
                        // Set to false so that the it loops to the other storage facilities
                        added = false;
                    }
                    i += 1;
                }

                // If added is still false
                if (! added)
                {
                    throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot add food, temperature is out of range");
                }
                else
                {
                    // Update class fields
                    updateInventoryCount();
                }
            }
        }
    }

    public String removeItem(String position)
    {
        // NAME:        removeItem
        // PURPOSE:     Remove item from a a storage facility
        // ASSERTION:   Item will be removed, unless it doesn't exist
        // IMPORTS:     position (String)
        // EXPORTS:     outputString (String)

        int storageType, storagePosition;
        String outputString = "";

        // If storage has not yet been constructed from file
        if (! isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Storage: Cannot remove food until read from file");
        }
        else
        {
            // Update class fields
            updateInventoryCount();

            // If position is either null or empty
            if (! validateString(position))
            {
                throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot remove food, position is either null or empty");
            }
            // If there is nothing in the storage
            else if (numTotalUsed == 0)
            {
                throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot remove food, storage is empty");
            }
            else
            {
                // Split position at ","
                String[] splitString = position.split(",");

                // If position is not valid
                if (splitString.length != 2)
                {
                    throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot remove food, position is not a valid format");
                }
                else
                {
                    // Set storage facility location and slot number
                    storageType = Integer.parseInt(splitString[0]);
                    storagePosition = Integer.parseInt(splitString[1]);

                    // If storageType or storagePosition goes out of bounds of the array
                    if (
                        (
                            storageType >= storage.length
                        ) || (
                            storagePosition >= numFreezer
                        ) || (
                            storagePosition >= numFridge
                        ) || (
                            storagePosition >= numPantry
                        )
                    )
                    {
                        throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot remove food, storage type does not exist");
                    }
                    else
                    {
                        // Set output message
                        outputString = "Deleted item at " + storageType + "," + storagePosition + "\n\n"
                                     + "==============================\n"
                                     + "Item details:\n"
                                     + "==============================\n"
                                     + storage[storageType][storagePosition].toString();

                        // Remove element from array and reshuffle
                        storage[storageType] = deleteElement(storage[storageType], storagePosition);

                        // Update class fields
                        updateInventoryCount();
                    }
                }
            }
        }

        return outputString;
    }

    public String[] exportFoodStorage()
    {
        // NAME:        exportFoodStorage
        // PURPOSE:     Export the storage and food as a csv file
        // ASSERTION:   Return a string containing all information in this object
        // IMPORTS:     none
        // EXPORTS:     exportStringSplit (String array)

        String exportString;
        String[] exportStringSplit;

        // If storage has not been constructed from file
        if (! isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot write contents of storage until read from file");
        }
        else
        {
            // Write the first entry to exportString
            exportString = storage[0][0].getName() + "," + storage[0].length;

            // For each storage facility
            for (int i = 1; i < storage.length; i++)
            {
                // Append to exportString
                exportString += "\n"
                              + storage[i][0].getName() + "," + storage[i].length;
            }

            // Append newline to seperate Storage and Food
            exportString += "\n";

            // For each storage facility
            for (int i = 0; i < storage.length; i++)
            {
                // For each storage facility slot
                for (int j = 0; j < storage[i].length; j++)
                {
                    // If there is an item
                    if (storage[i][j].getHasItem())
                    {
                        // Call the toCSV method from food item in storage
                        exportString += "\n"
                                      + storage[i][j].toCSV();
                    }
                }
            }
        }

        exportStringSplit = exportString.split("\n");
        return exportStringSplit;
    }

    public String findExpired()
    {
        // NAME:        findExpired
        // PURPOSE:     Finds all the expired food in storage
        // ASSERTION:   Returns a string containing location and details on the expired food
        // IMPORTS:     none
        // EXPORTS:     exportExpired (String)

        String exportExpired;
        int upperLimit;

        exportExpired = "";

        // If storage has not been constructed from file
        if (! isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot find expired items until read from file");
        }
        else
        {
            // For each storage facility
            for (int i = 0; i < storage.length; i++)
            {
                // Get number of items in storage
                upperLimit = calcFull(storage[i]);

                // Append header to string
                exportExpired += "\n\n"
                               + "==============================\n"
                               + "Display expired items in " + storage[i][0].getName() + "\n"
                               + "==============================\n\n";

                // For each items in storage facility
                for (int j = 0; j < upperLimit; j++)
                {
                    // If item is expired
                    if (storage[i][j].isExpired())
                    {
                        // Create copy of item
                        Food expiredItem = storage[i][j].getItem();

                        // Call toString method in item and append to string
                        exportExpired += "\n"
                                       + "==============================\n"
                                       + "Index: " + i + ", " + j + "\n"
                                       + "==============================\n"
                                       + expiredItem.toString() + "\n";
                    }
                }
            }
        }

        return exportExpired;
    }

    // Getters
    public boolean getConstructFromFile() { return isConstructFromFile; }
    public int getNumFreezer() { return numFreezer; } 
    public int getNumFridge() { return numFridge; } 
    public int getNumPantry() { return numPantry; } 
    public int getNumStorage() { return numStorage; } 
    public int getNumFreezerUsed() { return numFreezerUsed; } 
    public int getNumFridgeUsed() { return numFridgeUsed; } 
    public int getNumPantryUsed() { return numPantryUsed; } 
    public int getNumTotalUsed() { return numTotalUsed; } 

    public int getNumMeat() { return numMeat; } 
    public int getNumGrain() { return numGrain; } 
    public int getNumFruit() { return numFruit; } 
    public int getNumVegetable() { return numVegetable; } 
    public int getNumFood() { return numFood; } 

    public Storage[][] getStorage()
    {
        // NAME:        getStorage
        // PURPOSE:     Deep copy the storage array
        // ASSERTION:   Return a copy of the storage 2d array
        // IMPORTS:     none
        // EXPORTS:     outStorage (Storage 2d array)

        Storage[][] outStorage = new Storage[3][0];

        // For each storage facility
        for (int i = 0; i < storage.length; i++)
        {
            // Create an array of detected storage facility
            if (storage[i][0] instanceof Freezer)
            {
                outStorage[i] = new Freezer[numFreezer];
            }
            else if (storage[i][0] instanceof Fridge)
            {
                outStorage[i] = new Fridge[numFridge];
            }
            else if (storage[i][0] instanceof Pantry)
            {
                outStorage[i] = new Pantry[numPantry];
            }
        }

        // For each item in new storage facility
        for (int i = 0; i < outStorage.length; i++)
        {
            // For each element in new storage facility slot
            for (int j = 0; j < outStorage[i].length; j++)
            {
                // Clone the contents of the original storage to new storage
                outStorage[i][j] = storage[i][j].clone();
            }
        }

        return outStorage;
    }

    public Food[] getFood()
    {
        // NAME:        getFood
        // PURPOSE:     Deep copy the food array
        // ASSERTION:   Return a copy of the food array
        // IMPORTS:     none
        // EXPORTS:     outFood

        Food[] outFood = new Food[numFood];

        // For each element in food array
        for (int i = 0; i < food.length; i++)
        {
            // Clone food item to new food array
            outFood[i] = food[i].clone();
        }

        return outFood;
    }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Check if inObj is the same as this object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is FoodStorage
        if (inObj instanceof FoodStorage)
        {
            FoodStorage inFoodStorage = (FoodStorage)inObj;
            isEquals = (
                (
                    isConstructFromFile == inFoodStorage.getConstructFromFile()
                ) && (
                    numFreezer == inFoodStorage.getNumFreezer()
                ) && (
                    numFridge == inFoodStorage.getNumFridge()
                ) && (
                    numPantry == inFoodStorage.getNumPantry()
                ) && (
                    numStorage == inFoodStorage.getNumStorage()
                ) && (
                    numFreezerUsed == inFoodStorage.getNumFreezerUsed()
                ) && (
                    numFridgeUsed == inFoodStorage.getNumFridgeUsed()
                ) && (
                    numPantryUsed == inFoodStorage.getNumPantryUsed()
                ) && (
                    numTotalUsed == inFoodStorage.getNumTotalUsed()
                ) && (
                    numMeat == inFoodStorage.getNumMeat()
                ) && (
                    numGrain == inFoodStorage.getNumGrain()
                ) && (
                    numFruit == inFoodStorage.getNumFruit()
                ) && (
                    numVegetable == inFoodStorage.getNumVegetable()
                ) && (
                    compareStorage(storage, inFoodStorage.getStorage())
                ) && (
                    compareFood(food, inFoodStorage.getFood())
                )
            );
        }

        return isEquals;
    }

    public String displayStorage(String inStorageName)
    {
        // NAME:        toString
        // PURPOSE:     Print the contents of a specified storage facility
        // ASSERTION:   Return a string containing all of the food items in the storage facility
        // IMPORTS:     inStorageName (String)
        // EXPORTS:     str (String)

        int index, upperLimit;
        String name, str = "";

        // If storage has not been constructed from file
        if (! isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Food Storage: Cannot display until add from file");
        }
        else
        {
            // If inStorageName is null or empty
            if (! validateString(inStorageName))
            {
                throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot write contents of storage, storage name is either null or empty");
            }
            // If storage facility could not be found
            else if (! findStorage(inStorageName))
            {
                throw new IllegalArgumentException("Invalid Action in Food Storage: Cannot write contents of storage, storage name cannot be found");
            }
            else
            {
                // Get the index of the storage facility
                index = findStorageIndex(inStorageName);

                // Calculate the number of items in that storage facility and get name
                upperLimit = calcFull(storage[index]);
                name = storage[index][0].getName();

                // Create the header depending on the storage facility
                str = "Location: " + name + "\n"
                    + "Total space: " + storage[index].length + "\n"
                    + "Total used: " + upperLimit + "\n"
                    + "==============================\n"
                    + "Contents of " + name + "\n"
                    + "==============================\n\n";

                // For each item in that storage facility
                for (int i = 0; i < storage[index].length; i++)
                {
                    // If it contains an item
                    if (storage[index][i].getHasItem())
                    {
                        // Call the toString method
                        str += "==============================\n"
                             + "Item no. " + (i + 1) + "\n"
                             + "==============================\n"
                             + storage[index][i].toString() + "\n\n";
                    }
                }
            }
        }

        return str;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Print the contents of the storage facility
        // ASSERTION:   Return a string containing all of the food items in the storage facility
        // IMPORTS:     none
        // EXPORTS:     str (String)

        int usedSpace;
        String name, str = "";

        // If storage has not been constructed from file
        if (! isConstructFromFile)
        {
            throw new IllegalArgumentException("Invalid Food Storage: Cannot display until add from file");
        }
        else
        {
            for (int i = 0; i < storage.length; i++)
            {
                // Calculate the number of items in that storage facility
                usedSpace = calcFull(storage[i]);
                name = storage[i][0].getName();

                // Create the header depending on the storage facility
                str = "Location: " + name + "\n"
                    + "Total space: " + storage[i].length + "\n"
                    + "Total used: " + usedSpace + "\n"
                    + "==============================\n"
                    + "Contents of " + name + "\n"
                    + "==============================\n\n";

                // For each item in that storage facility
                for (int j = 0; j < storage[i].length; j++)
                {
                    // If it contains an item
                    if (storage[i][j].getHasItem())
                    {
                        // Call the toString method
                        str += "==============================\n"
                             + "Item no. " + (j + 1) + "\n"
                             + "==============================\n"
                             + storage[i][j].toString() + "\n\n";
                    }
                }
            }
        }

        return str;
    }

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check if inString is null or empty
        // ASSERTION:   Returns true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean compareStorage(Storage[][] storageOne, Storage[][] storageTwo)
    {
        // NAME:        compareStorage
        // PURPOSE:     Deep compares the contents of two 2d arrays
        // ASSERTION:   Return true or false
        // IMPORTS:     storageOne (Storage 2d array), storageTwo (Storage 2d array)
        // EXPORTS:     isEquals

        boolean isEquals = true;
        int i, j;
        i = 0;
        j = 0;

        // If the two arrays are the same length
        if (storageOne.length == storageTwo.length)
        {
            // While isEquals is true and i is less than the length of the array
            while ((isEquals) && (i < storageOne.length))
            {
                // Check if the set amount of length is the same on both arrays
                isEquals = (storageOne[i].length == storageTwo[i].length);
                i += 1;
            }

            // Reset indexes
            i = 0;
            j = 0;

            // While isEquals is true and i is less than the length of the array
            while ((isEquals) && (i < storageOne.length))
            {
                // While isEquals is true and j is less than the length of the array
                while ((isEquals) && (j < storageOne[i].length))
                {
                    // Compare using the equals method in Storage object
                    isEquals = storageOne[i][j].equals(storageTwo[i][j]);
                    j += 1;
                }
                i += 1;
            }
        }

        return isEquals;
    }

    private boolean compareFood(Food[] foodOne, Food[] foodTwo)
    {
        // NAME:        compareFood
        // PURPOSE:     Compare the contents of food array to another array
        // ASSERTION:   Return true or false
        // IMPORTS:     foodOne (Food array), foodTwo (Food array)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = true;
        int i;
        i = 0;

        // If the lengths of the two arrays are the same
        if (foodOne.length == foodTwo.length)
        {
            // While isEquals is true and i is less that foodOne length
            while ((isEquals) && (i < foodOne.length))
            {
                // Use the equals method in Food object
                isEquals = (foodOne[i].equals(foodTwo[i]));
                i += 1;
            }
        }

        return isEquals;
    }

    private Storage[] deleteElement(Storage[] inStorageCell, int positionCell)
    {
        // NAME:        deleteElement
        // PURPOSE:     Delete an object in Storage array and reshuffle
        // ASSERTION:   Storage array will have one food item removed
        // IMPORTS:     inStorageCell (Storage array), positionCell (int)
        // EXPORTS:     tempArray (Storage array)

        // Create a temporary array with the same size as inStorageCell
        Storage[] tempArray = new Storage[inStorageCell.length];

        // For i increment until positionCell
        for (int i = 0; i < positionCell; i++)
        {
            // Clone array over from inStorageCell to tempArray
            tempArray[i] = inStorageCell[i].clone();
        }

        // For i starting at the skipped entry to the end of inStorageCell
        for (int i = positionCell; i < inStorageCell.length - 1; i++)
        {
            // Clone array over from inStorageCell to tempArray
            tempArray[i] = inStorageCell[i + 1].clone();
        }

        // Have the last slot in tempArray be set a new placeholder Storage object
        tempArray[inStorageCell.length - 1] = inStorageCell[0].placeholder();
        return tempArray;
    }

    private boolean findStorage(String search)
    {
        // NAME:        findStorage
        // PURPOSE:     Find the storage facility by string
        // ASSERTION:   Return true or false
        // IMPORTS:     search (String)
        // EXPORTS:     match (boolean)

        boolean match = false;
        int i;
        i = 0;

        // While match is not found and i is less than length of storage array
        while ((! match) && (i < storage.length))
        {
            // See if the name is the same as the search entry
            match = (search.equals(storage[i][0].getName()));
            i += 1;
        }

        return match;
    }

    private int findStorageIndex(String search)
    {
        // NAME:        findStorageIndex
        // PURPOSE:     Find the storage facility by string
        // ASSERTION:   Return the index number of the storage facility
        // IMPORTS:     search (String)
        // EXPORTS:     i (int)

        // Same logic as above, except return i instead
        boolean match = false;
        int i;
        i = 0;

        while ((! match) && (i < storage.length))
        {
            match = (search.equals(storage[i][0].getName()));
            i += 1;
        }
        i -= 1;
        return i;
    }

    private void updateInventoryCount()
    {
        // NAME:        updateInventoryCount
        // PURPOSE:     Update the class fields after any changes to the storage array
        // ASSERTION:   All of the counts of each objects will get updated
        // IMPORTS:     none
        // EXPORTS:     none

        // Set them all to 0
        numFreezer = 0;
        numFridge = 0;
        numPantry = 0;
        numStorage = 0;
        numFreezerUsed = 0;
        numFridgeUsed = 0;
        numPantryUsed = 0;
        numTotalUsed = 0;

        numMeat = 0;
        numGrain = 0;
        numFruit = 0;
        numVegetable = 0;
        numFood = 0;

        // For each storage facility
        for (int i = 0; i < storage.length; i++)
        {
            // Determine which storage facility it is and increment it's variable respectively
            if (storage[i][0] instanceof Freezer)
            {
                numFreezer = storage[i].length;
                numFreezerUsed = calcFull(storage[i]);
            }
            else if (storage[i][0] instanceof Fridge)
            {
                numFridge = storage[i].length;
                numFridgeUsed = calcFull(storage[i]);
            }
            else if (storage[i][0] instanceof Pantry)
            {
                numPantry = storage[i].length;
                numPantryUsed = calcFull(storage[i]);
            }

            // Counts the different types of food in the storage facility
            numMeat += countFood(storage[i], 'M');
            numGrain += countFood(storage[i], 'G');
            numFruit += countFood(storage[i], 'F');
            numVegetable += countFood(storage[i], 'V');
        }

        // Total up storage, used storage and food
        numStorage = numFreezer + numFridge + numPantry;
        numTotalUsed = numFreezerUsed + numFridgeUsed + numPantryUsed;
        numFood = numMeat + numGrain + numFruit + numVegetable;
    }

    private int countFood(Storage[] inStorageCell, char search)
    {
        // NAME:        countFood
        // PURPOSE:     Count the number of food specified in a storage facility
        // ASSERTION:   Return count
        // IMPORTS:     inStorageCell (Storage array), search (char)
        // EXPORTS:     count (int)

        int count;
        count = 0;

        // For each slot in inStorageCell
        for (int i = 0; i < inStorageCell.length; i++)
        {
            // If it contains an item
            if (inStorageCell[i].getHasItem())
            {
                // Check to see if its the correct type of food
                if (search == inStorageCell[i].getItem().getItemType())
                {
                    count += 1;
                }
            }
        }

        return count;
    }

    private int calcFull(Storage[] inStorageCell)
    {
        // NAME:        calcFull
        // PURPOSE:     Calculate the number of used slots in storage facility
        // ASSERTION:   Return count of food in storage facility
        // IMPORTS:     inStorageCell (Storage array)
        // EXPORTS:     count (int)

        int count = 0;

        // For each item in inStorageCell  
        for (int i = 0; i < inStorageCell.length; i++)
        {
            // If it contains an item, increment count
            if (inStorageCell[i].getHasItem())
            {
                count += 1;
            }
        }

        return count;
    }
}
