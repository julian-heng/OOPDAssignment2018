import java.util.*;

public class Grain extends Food implements IFood
{
    /**
     *  CLASSNAME:      Grain
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Create a grain object containing characteristics of the food item
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASSFIELDS:
     *      - type (String)
     *      - volume (double) 
     *      - expireBy (Date)
     *      - expired (boolean)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor
     *      - Copy Constructor
     *      - calcExpire
     *      - toCSV
     *      - setType
     *      - setVolume
     *      - setExpireBy
     *      - getType
     *      - getWeight
     *      - getExpireBy
     *      - getItemTypeStr
     *      - getItemType
     *      - clone
     *      - equals
     *      - toString
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateReal
     *      - validateObject
     *      - validateDate
     *      - realCompare
     **/

    // Declare class fields
    private String type;
    private double volume;
    private Date expireBy;
    private boolean expired;

    // Constructors
    public Grain()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a grain object with placeholder values
        // ASSERTION:   Default grain object will be created
        // IMPORTS:     none
        // EXPORTS:     none

        // Call super class
        super();

        // Set default values
        type = "Default type";
        volume = 10.0;
        expireBy = new Date();
        expired = validateDate(expireBy);
    }

    public Grain(String inName, double inTempStorage, String inPackageType, String inType, double inVolume, Date inExpireBy)
    {
        // NAME:        Alternate Constructor
        // PURPOSE:     Create a grain object with imports
        // ASSERTION:   Grain object with imports will be created
        // IMPORTS:     inName (String), intempStorage (double), inPackageType (String), inType (String), inVolume (double), inExpireBy (Date)
        // EXPORTS:     none

        // Call super class
        super(inName, inTempStorage, inPackageType);

        // Call setters
        setType(inType);
        setVolume(inVolume);
        setExpireBy(inExpireBy);
    }

    public Grain(Grain inGrain)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Copy the contents of the grain import into a new object
        // ASSERTION:   Copy inGrain object to new grain object
        // IMPORTS:     inGrain (Grain)
        // EXPORTS:     none

        // Call super class
        super(inGrain);

        // Call getters in inGrain
        type = inGrain.getType();
        volume = inGrain.getVolume();
        expireBy = inGrain.getExpireBy();
        expired = inGrain.getExpired();
    }

    // Mutators
    @Override
    public boolean calcExpire(Date today)
    {
        // NAME:        calcExpire
        // PURPOSE:     Determine if the object expire date has passed current date
        // ASSERTION:   Return true or false
        // IMPORTS:     today (Date)
        // EXPORTS:     isExpire (boolean)

        boolean isExpire = false;
        isExpire = today.isBehind(expireBy);
        return isExpire;
    }

    @Override
    public String toCSV()
    {
        // NAME:        toCSV
        // PURPOSE:     Combine the contents of this object into a csv line
        // ASSERTION:   Return csv line from this object
        // IMPORTS:     none
        // EXPORTS:     csvLine (String)

        String csvLine;

        // Assemble csv line
        csvLine = getItemTypeStr() + "," 
                + super.getName() + "," 
                + type + "," 
                + volume + "," 
                + super.getTempStorage() + ","
                + expireBy.toString() + ","
                + super.getPackageType();
        return csvLine;
    }

    // Setters
    public void setType(String inType)
    {
        // NAME:        setCut
        // PURPOSE:     Change the cut variable to inType
        // ASSERTION:   cut will be replaced by inType
        // IMPORTS:     inType (String)
        // EXPORTS:     none

        // If inType is either null or empty
        if (! validateString(inType))
        {
            throw new IllegalArgumentException("Invalid Grain: Cannot set type, is either null or empty");
        }
        else
        {
            type = inType;
        }
    }

    public void setVolume(double inVolume)
    {
        // NAME:        setVolume
        // PURPOSE:     Change the volume variable to inVolume
        // ASSERTION:   volume will be replaced by inVolume
        // IMPORTS:     inVolume (double)
        // EXPORTS:     none

        // If inVolume is not in between 0.2 and 5.0
        if (! validateReal(inVolume))
        {
            throw new IllegalArgumentException("Invalid Grain: Cannot set volume, it is not in between 0.2 and 5.0");
        }
        else
        {
            volume = inVolume;
        }
    }

    public void setExpireBy(Date inExpireBy)
    {
        // NAME:        setExpireBy
        // PURPOSE:     Change the expireBy date object to inExpireBy
        // ASSERTION:   expireBy will be replaced by inExpireBy
        // IMPORTS:     inExpireBy (Date)
        // EXPORTS:     none

        // If either inExpireBy is null or date is in the past
        if ((! validateObject(inExpireBy)) || (validateDate(inExpireBy)))
        {
            throw new IllegalArgumentException("Invalid Grain: Cannot set expire by date, not valid");
        }
        else
        {
            expireBy = new Date(inExpireBy);
            expired = validateDate(expireBy);
        }
    }

    // Getters
    public String getType() { return type; }
    public double getVolume() { return volume; }
    public Date getExpireBy() { return new Date(expireBy); }
    public boolean getExpired() { return expired; }

    @Override
    public String getItemTypeStr() { return "Grain"; }

    @Override
    public char getItemType() { return 'G'; }

    @Override
    public Grain clone()
    {
        // NAME:        clone
        // PURPOSE:     Clone this object
        // ASSERTION:   A clone will be made
        // IMPORTS:     none
        // EXPORTS:     cloneGrain (Grain)

        Grain cloneGrain = new Grain(this);
        return cloneGrain;
    }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Compare inObj to current object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a grain object
        if (inObj instanceof Grain)
        {
            Grain inGrain = (Grain)inObj;
            isEquals = (
                (
                    super.equals(inGrain)
                ) && (
                    type.equals(inGrain.getType())
                ) && (
                    realCompare(volume, inGrain.getVolume())
                ) && (
                    expireBy.equals(inGrain.getExpireBy())
                ) && (
                    expired == inGrain.getExpired()
                )
            );
        }
        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Convert all the contents of grain object to a string
        // ASSERTION:   Return a string with all characteristics to this object
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = "Item type: " + getItemTypeStr() + "\n"
            + super.toString() + "\n"
            + "Type: " + type + "\n"
            + "Volume: " + volume + "\n"
            + "Expire by: " + expireBy.toString() + "\n"
            + "Expired: " + expired;
        return str;
    }

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     none

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean validateReal(double inReal)
    {
        // NAME:        validateReal
        // PURPOSE:     Check if inReal is more than 0.2 but less than 5.0
        // ASSERTION:   Return true or false
        // IMPORTS:     inReal (double)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (
            (
                (inReal > 0.2) || (realCompare(inReal, 0.2))
            ) && (
                (inReal < 5.0) || (realCompare(inReal, 5.0))
            )
        );
        return isValid;
    }

    private boolean validateObject(Object inObject)
    {
        // NAME:        validateObject
        // PURPOSE:     Check if inObject is null
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (inObject != null);
        return isValid;
    }

    private boolean validateDate(Date inExpireBy)
    {
        // NAME:        validateDate
        // PURPOSE:     Check if date is not in the past
        // ASSERTION:   Return true or false
        // IMPORTS:     inExpireBy (Date)
        // EXPORTS:     isValid (boolean)

        Date today = new Date().setToday();
        boolean isValid = false;
        isValid = today.isBehind(inExpireBy);
        return isValid;
    }

    private boolean realCompare(double num1, double num2)
    {
        // NAME:        realCompare
        // PURPOSE:     Check if two real numbers are the same value
        // ASSERTION:   Return true or false
        // IMPORTS:     num1 (double), num2 (double)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;
        isEquals = (Math.abs(num1 - num2) < 0.0000001);
        return isEquals;
    }
}
