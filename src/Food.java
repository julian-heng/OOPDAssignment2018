import java.util.*;

public abstract class Food
{
    /**
     *  CLASSNAME:      Food
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Abstract class to Meat, Grain, Fruit and Vegetable
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASSFIELDS:
     *      - name (String)
     *      - tempStorage (double)
     *      - packageType (String)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor
     *      - Copy Constructor
     *      - setName
     *      - setTempStorage
     *      - setPackageType
     *      - getName
     *      - getTempStorage
     *      - getPackageType
     *      - equals
     *      - toString
     *      - clone
     *      - getItemTypeStr (Abstract)
     *      - getItemType (Abstract)
     *      - calcExpire (Abstract)
     *      - toCSV (Abstract)
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - compareReal
     **/

    // Declare class fields
    private String name, packageType;
    private double tempStorage;

    // Constructors
    public Food()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a default food object
        // ASSERTION:   Default food object with placeholder values
        // IMPORTS:     none
        // EXPORTS:     none

        name = "Food";
        tempStorage = 10.0;
        packageType = "Box";
    }

    public Food(String inName, double inTempStorage, String inPackageType)
    {
        // NAME:        Alternate Constructor
        // PURPOSE:     Create a food object with imports
        // ASSERTION:   A food object with import values
        // IMPORTS:     inName (String), inTempStorage (double), inPackageType (String)
        // EXPORTS:     none

        // Call setters to set values of object
        setName(inName);
        setTempStorage(inTempStorage);
        setPackageType(inPackageType);
    }

    public Food(Food inFood)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Create a copy of import food object
        // ASSERTION:   Copy object will be created
        // IMPORTS:     inFood (Food)
        // EXPORTS:     none

        // Call getters in inFood
        name = inFood.getName();
        tempStorage = inFood.getTempStorage();
        packageType = inFood.getPackageType();
    }

    // Setters
    public void setName(String inName)
    {
        // NAME:        setName
        // PURPOSE:     Change the name variable
        // ASSERTION:   name variable will be set to inName
        // IMPORTS:     inName (String)
        // EXPORTS:     none

        // If inName is null or empty
        if (! validateString(inName))
        {
            throw new IllegalArgumentException("Invalid Food: Name is either null or empty");
        }
        else
        {
            name = inName;
        }
    }

    public void setTempStorage(double inTempStorage)
    {
        // NAME:        setTempStorage
        // PURPOSE:     Changet the tempStorage variable
        // ASSERTION:   tempStorage variable will be set to inTempStorage
        // IMPORTS:     inTempStorage (double)
        // EXPORTS:     none

        // No checks for double
        tempStorage = inTempStorage;
    }

    public void setPackageType(String inPackageType)
    {
        // NAME:        setPackageType
        // PURPOSE:     Change the packageType variable
        // ASSERTION:   packageType variable will be set to inPackageType
        // IMPORTS:     inPackageType (String)
        // EXPORTS:     none

        // If inPackageType is null or empty
        if (! validateString(inPackageType))
        {
            throw new IllegalArgumentException("Invalid Food: Package type is either null or invalid");
        }
        else
        {
            packageType = inPackageType;
        }
    }

    // Getters
    public String getName() { return name; }
    public double getTempStorage() { return tempStorage; }
    public String getPackageType() { return packageType; }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Check if inObj have the same contents as this object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a Food object
        if (inObj instanceof Food)
        {
            Food inFood = (Food)inObj;
            isEquals = (
                (
                    name.equals(inFood.getName())
                ) && (
                    compareReal(tempStorage, inFood.getTempStorage())
                ) && (
                    packageType.equals(inFood.getPackageType())
                )
            );
        }

        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Export the contents of this object as a string
        // ASSERTION:   Return a string with the contents of this object
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = "Name: " + name + "\n"
            + "Storage temperature: " + tempStorage + "\n"
            + "Package Type: " + packageType;
        return str;
    }

    // Abstract methods
    public abstract Food clone();
    public abstract String getItemTypeStr();
    public abstract char getItemType();
    public abstract boolean calcExpire(Date today);
    public abstract String toCSV();

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     Check if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean compareReal(double num1, double num2)
    {
        // NAME:        compareReal
        // PURPOSE:     Check if two real numbers are the same
        // ASSERTION:   return isEquals
        // IMPORTS:     num1 (double), num2 (double)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;
        isEquals = (Math.abs(num1 - num2) < 0.0000001);
        return isEquals;
    }
}
