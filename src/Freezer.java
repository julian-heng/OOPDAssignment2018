import java.util.*;

public class Freezer extends Storage
{
    /**
     *  CLASSNAME:      Freezer
     *  AUTHOR:         Julian Heng (19473701)
     *  PURPOSE:        Storage facility to store items with temperatures -27.0 to -5.0
     *  FIRST CREATED:  21/05/2018
     *  LAST MODIFIED:  28/05/2018
     *
     *  CLASS FIELDS:
     *      - LOWERTEMP (double Constant)
     *      - UPPERTEMP (double Constant)
     *      - name (String)
     *      - item (Food)
     *      - hasItem (boolean)
     *
     *  PUBLIC METHODS:
     *      - Default Constructor
     *      - Alternate Constructor (No Item)
     *      - Alternate Constructor (Item)
     *      - Copy Constructor
     *      - isExpired
     *      - toCSV
     *      - canAdd
     *      - setName
     *      - setItem
     *      - getLower
     *      - getUpper
     *      - getName
     *      - getItem
     *      - getHasItem
     *      - equals
     *      - toString
     *      - clone
     *      - placeholder
     *  
     *  PRIVATE METHODS:
     *      - validateString
     *      - validateObject
     *      - validateTemp
     *      - realCompare
     **/

    // Declare class fields
    public static final double LOWERTEMP = -27.0, UPPERTEMP = -5.0;
    private String name;
    private Food item;
    private boolean hasItem;

    // Constructors
    public Freezer()
    {
        // NAME:        Default Constructor
        // PURPOSE:     Create a Freezer with placeholder values
        // ASSERTION:   Freezer object with default values is created
        // IMPORTS:     none
        // EXPORTS:     none

        // Set default values
        name = "Placeholder";
        item = new Meat();
    }

    public Freezer(String inName)
    {
        // NAME:        Alternate Constructor (No Item)
        // PURPOSE:     Create a Freezer with imports
        // ASSERTION:   Freezer object with import value is created
        // IMPORTS:     inName (String)
        // EXPORTS:     none

        // Call setters to set values
        setName(inName);

        // Create a placeholder food item
        item = new Meat();
    }

    public Freezer(String inName, Food inItem)
    {
        // NAME:        Alternate Constructor (Item)
        // PURPOSE:     Create a Freezer with imports
        // ASSERTION:   Freezer object with import value is created
        // IMPORTS:     inName (String), inItem (Food)
        // EXPORTS:     none

        // Call setters to set values
        setName(inName);
        setItem(inItem);
    }

    public Freezer(Freezer inFreezer)
    {
        // NAME:        Copy Constructor
        // PURPOSE:     Copy the contents of inFreezer to this object
        // ASSERTION:   Copy of inFreezer will be created
        // IMPORTS:     inFreezer (Freezer)
        // EXPORTS:     none

        // Call getters in inFreezer
        name = inFreezer.getName();
        item = inFreezer.getItem();
        hasItem = inFreezer.getHasItem();
    }

    public boolean isExpired()
    {
        // NAME:        isExpired
        // PURPOSE:     Check if the item in the Freezer is expired
        // ASSERTION:   Return true or false
        // IMPORTS:     none
        // EXPORTS:     isExpired (boolean)

        boolean isExpired = false;

        // Create a Date object with today's date
        Date today = new Date().setToday();

        // Call the calcExpired method in the item
        isExpired = (item.calcExpire(today));
        return isExpired;
    }

    // Mutators
    @Override
    public String toCSV()
    {
        // NAME:        toCSV
        // PURPOSE:     Return the item in Freezer as a csv line
        // ASSERTION:   Call the toCSV method in the item and pass it through
        // IMPORTS:     none
        // EXPORTS:     itemCSV

        String itemCSV;
        itemCSV = item.toCSV();
        return itemCSV;
    }

    @Override
    public boolean canAdd(Food inFood)
    {
        // NAME:        canAdd
        // PURPOSE:     Check if the food item can be stored in the Freezer
        // ASSERTION:   Returns true or false
        // IMPORTS:     inFood (Food)
        // EXPORTS:     add (boolean)

        boolean add = false;

        // If inFood is null
        if (! validateObject(inFood))
        {
            throw new IllegalArgumentException("Invalid Freezer: Item is not valid, cannot check if can be added");
        }
        else
        {
            // Check if inFood can be stored
            add = (validateTemp(inFood));
        }

        return add;
    }

    public void setName(String inName)
    {
        // NAME:        setName
        // PURPOSE:     Change the name variable
        // ASSERTION:   name will be replaced by inName
        // IMPORTS:     inName (String)
        // EXPORTS:     none

        // If inName is null or empty
        if (! validateString(inName))
        {
            throw new IllegalArgumentException("Invalid Freezer: Name is either null or empty");
        }
        else
        {
            name = inName;
        }
    }

    @Override
    public void setItem(Food inItem)
    {
        // NAME:        setItem
        // PURPOSE:     Change the item variable
        // ASSERTION:   item will be replaced by inItem
        // IMPORTS:     inItem (Food)
        // EXPORTS:     none

        // If inItem is null
        if (! validateObject(inItem))
        {
            throw new IllegalArgumentException("Invalid Freezer: Item is not valid");
        }
        // If inItem's storage temp is out of range
        else if (! validateTemp(inItem))
        {
            throw new IllegalArgumentException("Invalid Freezer: Item cannot be stored, temperature is out of range");
        }
        else
        {
            item = inItem.clone();
            hasItem = true;
        }
    }

    // Getters
    @Override
    public double getLower() { return LOWERTEMP; }
    @Override
    public double getUpper() { return UPPERTEMP; }
    @Override
    public String getName() { return name; }
    @Override
    public Food getItem() { return item.clone(); }
    @Override
    public boolean getHasItem() { return hasItem; }

    public boolean equals(Object inObj)
    {
        // NAME:        equals
        // PURPOSE:     Compare inObj to this object
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;

        // If inObj is a Freezer
        if (inObj instanceof Freezer)
        {
            Freezer inFreezer = (Freezer)inObj;
            isEquals = (
                (
                    realCompare(LOWERTEMP, inFreezer.getLower())
                ) && (
                    realCompare(UPPERTEMP, inFreezer.getUpper())
                ) && (
                    name.equals(inFreezer.getName())
                ) && (
                    item.equals(inFreezer.getItem())
                )
            );
        }

        return isEquals;
    }

    public String toString()
    {
        // NAME:        toString
        // PURPOSE:     Assemble the contents of this object into a string
        // ASSERTION:   Return a string of all the variables in Freezer as well as item
        // IMPORTS:     none
        // EXPORTS:     str (String)

        String str;
        str = "Storage type: " + name + "\n"
            + "Lower storage temp: " + LOWERTEMP + "\n"
            + "Upper storage temp: " + UPPERTEMP + "\n"
            + item.toString();
        return str;
    }

    @Override
    public Freezer clone()
    {
        // NAME:        clone
        // PURPOSE:     Clone this object
        // ASSERTION:   A clone will be made
        // IMPORTS:     none
        // EXPORTS:     cloneFreezer (Freezer)

        Freezer cloneFreezer = new Freezer(this);
        return cloneFreezer;
    }

    @Override
    public Freezer placeholder()
    {
        // NAME:        placeholder
        // PURPOSE:     Create a placeholder Freezer
        // ASSERTION:   Placeholder Freezer object will be created
        // IMPORTS:     none
        // EXPORTS:     placeholder (Freezer)

        Freezer placeholder = new Freezer("Freezer");
        return placeholder;
    }

    // Private methods
    private boolean validateString(String inString)
    {
        // NAME:        validateString
        // PURPOSE:     See if inString is null or empty
        // ASSERTION:   Return true or false
        // IMPORTS:     inString (String)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = ((inString != null) && (! inString.isEmpty()));
        return isValid;
    }

    private boolean validateObject(Object inObj)
    {
        // NAME:        validateObject
        // PURPOSE:     Check if inObj is null
        // ASSERTION:   Return true or false
        // IMPORTS:     inObj (Object)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (inObj != null);
        return isValid;
    }

    private boolean validateTemp(Food inItem)
    {
        // NAME:        validateTemp
        // PURPOSE:     Check if inItem's storage temp is within range
        // ASSERTION:   Return true or false
        // IMPORTS:     inItem (Food)
        // EXPORTS:     isValid (boolean)

        boolean isValid = false;
        isValid = (
            (
                (inItem.getTempStorage() > LOWERTEMP) || (realCompare(inItem.getTempStorage(), LOWERTEMP))
            ) && (
                (inItem.getTempStorage() < UPPERTEMP) || (realCompare(inItem.getTempStorage(), UPPERTEMP))
            )
        );
        return isValid;
    }

    private boolean realCompare(double num1, double num2)
    {
        // NAME:        realCompare
        // PURPOSE:     Check if two real numbers are equals
        // ASSERTION:   Return true or false
        // IMPORTS:     num1 (double), num2 (double)
        // EXPORTS:     isEquals (boolean)

        boolean isEquals = false;
        isEquals = (Math.abs(num1 - num2) < 0.0000001);
        return isEquals;
    }
}
