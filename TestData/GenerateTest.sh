#!/usr/bin/env bash

filename="./testGen.txt"
printf "%s\\n" "Pantry,$((RANDOM % 1000))" > "${filename}"
printf "%s\\n" "Freezer,$((RANDOM % 1000))" >> "${filename}"
printf "%s\\n\\n" "Fridge,$((RANDOM % 1000))" >> "${filename}"

food=(
    "Meat"
    "Grain"
    "Fruit"
    "Vegetable"
)

for ((i=0; i<"${1:-10}"; i++)); do
    index="$((RANDOM % 4))"
    if ((RANDOM % 2 == 0)); then
        negative="-"
    else
        negative=""
    fi

    case "${food[$index]}" in
        "Meat")
            printf "%s\\n" "Meat,name,type,$((RANDOM % 5)).$((RANDOM % 10)),${negative}$((RANDOM % 25)).${RANDOM},$((RANDOM % 28))/$((RANDOM % 12))/20$((RANDOM % 100)),Box" >> "${filename}"
        ;;
        "Grain")
            printf "%s\\n" "Grain,name,type,$((RANDOM % 5)).$((RANDOM % 10)),${negative}$((RANDOM % 25)).${RANDOM},$((RANDOM % 28))/$((RANDOM % 12))/20$((RANDOM % 100)),Box" >> "${filename}"
        ;;
        "Fruit")
            printf "%s\\n" "Fruit,name,type,$((RANDOM % 20)),${negative}$((RANDOM % 25)).${RANDOM},$((RANDOM % 28))/$((RANDOM % 12))/20$((RANDOM % 100)),Box" >> "${filename}"
        ;;
        "Vegetable")
            printf "%s\\n" "Vegetable,name,$((RANDOM % 5)).$((RANDOM % 10)),${negative}$((RANDOM % 25)).${RANDOM},$((RANDOM % 28))/$((RANDOM % 12))/20$((RANDOM % 100)),Box" >> "${filename}"
        ;;
    esac
done
