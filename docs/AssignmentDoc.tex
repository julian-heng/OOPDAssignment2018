\documentclass[10pt]{article}

\usepackage[a4paper, lmargin=25.4mm, rmargin=25.4mm, tmargin=20mm, bmargin=20mm]{geometry}
\usepackage{listings}
\usepackage[simplified]{pgf-umlcd}
\usepackage{color}
\usepackage{inconsolata}
\usepackage{multicol}
\usepackage{fancyhdr}
\usepackage{nameref}
\usepackage{enumitem}

\makeatletter
\newcommand*{\currentname}{\@currentlabelname}
\makeatother

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{codegray}{gray}{0.9}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\newcommand{\code}[1]{\small\texttt{#1}\normalsize}

\lstset
{
    % -- Settings -- %
    columns=flexible,
    xleftmargin=0.65cm,
    showstringspaces=false,
    breaklines=true,
    numbers=left,
    breakatwhitespace=true,
    % -- Looks -- %
    basicstyle=\linespread{0.5}\ttfamily,
    numberstyle=\ttfamily\color{gray},
    keywordstyle=\color{blue},
    commentstyle=\color{dkgreen},
    stringstyle=\color{mauve}
}

\pagestyle{fancy}
\fancyhf{}
\setlength\columnseprule{0.4pt}
\setlength{\parindent}{0pt}

\begin{document}
\setlist[enumerate]{itemsep=0mm}

\section{Design Philosophy}
\fancyhf[HL]{\footnotesize\currentname}
\fancyhf[FC]{\thepage}
\fancyhf[FL]{\footnotesize{Julian Heng [19473701]}}
\subsection{Description}

The design philosophy I used when developing this program is to keep in mind of what classes should be responsible of certain actions.
\\

Using the idea of class containment, I decided that FoodStorage should not create any Food or Storage objects.
One of the alternate constructors in FoodStorage can use an imported 2D Storage array, but not from any integer imports.
This causes the responsibility of creating the Food and Storage objects fall under FoodManager.
\\

For example, FoodStorage's responsibility does not include taking in user inputs, or output to screen.
Those responsibility belongs to FoodManager.
Thus, FoodStorage is created only to store and mutate the Food and Storage objects that it contains, while FoodManager only deals with user inputs and outputs.
\\

FoodStorage should not contain the method \code{calcExpiry()} to calculate the expiry date of a Food object, that responsibility belongs to both the Food objects and the Date class.
Storage should not contain a method to set the expiry date of the food object it contains, that responsibility belongs to FoodManager, FoodStorage and the Food objects.
\\

Using this design philosophy, it has caused more code reuse.
For example, the \code{calcExpiry()} method could reside in FoodStorage and call the getters in the Food Objects to compare them with, but instead, each food object has a \code{calcExpiry()} method instead.
However, by following this design philosophy, it is easier to see what characteristics the objects can contain and cannot contain.
FoodStorage contains both Food and Storage as abstract class.
Having a \code{calcExpiry()} method could be confusing as to know which objects it would use.
Thus, by moving the method into the individual Food objects, we can see that \code{calcExpiry()} is only used on Food objects and not Storage objects.
\\

Within the main class FoodManager, utility methods such as \code{inputInt()}, \code{getChoice()} and \code{readFile()} are separated into their own classes.
This is to reduce the scope of FoodManager as having those methods in one class can make it difficult to see the flow of events and logic.
In addition, the level of submodule calling should be at a minimum of 1 (entire program is contained within \code{runMenu()}) and should have a maximum of 5 levels deep.
Within FoodManager, readStorage splits off into 2 submodules, \code{processStorage()} and \code{processFood()}.
This helps keeping certain code contained into their own separated area within the program.
The benefit of doing so improves code readability.
\\

Other design philosophy relates to styles. In addition to following the provided Java style guide, I have also used my own styles for improved readability.
\begin{enumerate}
    \item{If statements containing long conditional statements will use multiple lines}
    \item{Setting a value to a boolean variable containing multiple conditional statements will use multiple lines}
    \item{Each submodule will have a comment header that details the name, purpose, assertion, imports and exports}
    \item{Line breaks to separate out sections of code that should improve readability (E.g. Line break after declaring variables, or a for-loop, etc)}
\end{enumerate}

As for the structure of different classes:
\begin{itemize}
    \item[-]{Structure of objects should have the constructors first, then the mutators, followed by the setters and getters, and then finished up with the private submodules.}
    \item[-]{Structure of FoodStorage is an exception to the structure of objects as I decided that it does not require setters for changing variables such as \code{numFreezer}, or \code{numFood}. These variables can only be changed by modifying the contents of the Food array or the 2D Storage array, to which calls \code{updateInventoryCount()} and change those variables.}
    \item[-]{Structure of the main class FoodManager follows the order of the menu.}
    \item[-]{Structure of the methods in utility classes should follow the order that they are called}
    \item[-]{Any \code{IllegalArgumentException} will get caught in a menu loop so that it will go back to main menu if an exception is raised.}
\end{itemize}

\newpage


\subsection{UML Diagram}

\begin{center}
    \begin{tikzpicture}{}[show background grid, font=\scriptsize\ttfamily]
        \begin{class}[text width=5.0cm]{FoodManager}{0,0}
        \end{class}

        \begin{class}[text width=5.0cm]{FoodStorage}{0,-1}
            \inherit{FoodManager}
            \attribute{- numFreezer: int}
            \attribute{- numFridge: int}
            \attribute{- numPantry: int}
            \attribute{- numStorage: int}
            \attribute{- numFreezerUsed: int}
            \attribute{- numFridgeUsed: int}
            \attribute{- numPantryUsed: int}
            \attribute{- numTotalUsed: int}
            \attribute{- numMeat: int}
            \attribute{- numGrain: int}
            \attribute{- numFruit: int}
            \attribute{- numVegetable: int}
            \attribute{- numFood: int}
            \attribute{- food: Food array}
            \attribute{- storage: 2D Storage array}
        \end{class}

        \begin{abstractclass}[text width=5.0cm]{Food}{-4,-6.5}
            \attribute{- name: String}
            \attribute{- tempStorage: double}
            \attribute{- packageType: String}
        \end{abstractclass}
    
        \begin{interface}[text width=5.0cm]{IFood}{-4,-10}
        \end{interface}
    
        \begin{class}[text width=5.0cm]{Meat}{-4, -11}
            \attribute{- cut: String}
            \attribute{- weight: double}
            \attribute{- useBy: Date}
            \attribute{- expired: boolean}
        \end{class}

        \begin{class}[text width=5.0cm]{Grain}{-4, -13.5}
            \attribute{- type: String}
            \attribute{- volume: double}
            \attribute{- expiredBy: Date}
            \attribute{- expired: boolean}
        \end{class}

        \begin{class}[text width=5.0cm]{Fruit}{-4, -16}
            \attribute{- type: String}
            \attribute{- numPieces: int}
            \attribute{- useBy: Date}
            \attribute{- expired: boolean}
        \end{class}

        \begin{class}[text width=5.0cm]{Vegetable}{-4, -18.5}
            \attribute{- weight: double}
            \attribute{- expireBy: Date}
            \attribute{- expired: boolean}
        \end{class}

        \begin{class}[text width=5.0cm]{Date}{6, -17}
            \attribute{- day: int}
            \attribute{- month: int}
            \attribute{- year: int}
        \end{class}

        \begin{abstractclass}[text width=5.0cm]{Storage}{4,-6.5}
        \end{abstractclass}
    
        \begin{class}[text width=5.0cm]{Freezer}{6, -9}
            \attribute{+ LOWERTEMP:= -27.0}
            \attribute{+ UPPERTEMP:= -5.0}
            \attribute{- name: String}
            \attribute{- item: Food}
            \attribute{- hasItem: boolean}
        \end{class}

        \begin{class}[text width=5.0cm]{Fridge}{6, -11.5}
            \attribute{+ LOWERTEMP:= -2.0}
            \attribute{+ UPPERTEMP:= 6.0}
            \attribute{- name: String}
            \attribute{- item: Food}
            \attribute{- hasItem: boolean}
        \end{class}

        \begin{class}[text width=5.0cm]{Pantry}{6, -14}
            \attribute{+ LOWERTEMP:= 8.0}
            \attribute{+ UPPERTEMP:= 25.0}
            \attribute{- name: String}
            \attribute{- item: Food}
            \attribute{- hasItem: boolean}
        \end{class}

        \aggregation{FoodStorage}{}{1}{Food}
        \aggregation{FoodStorage}{}{1}{Storage}

        \draw [umlcd style, dashed, fill=none, <-, >=open triangle 60]
            coordinate (start) at (IFood.west)
            (start) -- ++(-0.5,0)
            coordinate (FoodInterface);

        \draw [umlcd style, fill=none]
            coordinate (start) at (Date.west)
            (start) -| ++(-2,1)
            coordinate (DateAggregation);

        \draw [umlcd style, fill=none, <-, >=open triangle 60]
            coordinate (start) at (Food.south)
            (start) |- ++(-4,-1)
            coordinate (FoodAbstract);

        \foreach \startpoint in {Meat, Grain, Fruit, Vegetable}
            \draw [umlcd style, dashed, fill=none, >=open triangle 60]
                ([yshift=0.3cm]\startpoint.west) -| (FoodInterface);

        \foreach \startpoint in {Meat, Grain, Fruit, Vegetable}
            \draw [umlcd style, fill=none, ->, >=open triangle 60]
                (DateAggregation) |- (\startpoint.east);

        \foreach \startpoint in {Meat, Grain, Fruit, Vegetable}
            \draw [umlcd style, fill=none, >=open triangle 60]
                ([yshift=-0.3cm]\startpoint.west) -| (FoodAbstract);

        \foreach \startpoint in {Freezer, Fridge, Pantry}
            \draw [umlcd style, fill=none, ->, >=open triangle 60]
                (\startpoint.west) -| ([xshift=-1.5cm]Storage.south);

    \end{tikzpicture}
\end{center}
\newpage


\section{Source Code}
    \fancyhead[R]{\footnotesize{FoodManager.pseudocode}}
    \lstinputlisting{../pseudo/FoodManager.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{FoodManager.java}}
    \lstinputlisting[language=Java]{../src/FoodManager.java}\newpage


    \fancyhead[R]{\footnotesize{FoodStorage.pseudocode}}
    \lstinputlisting{../pseudo/FoodStorage.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{FoodStorage.java}}
    \lstinputlisting[language=Java]{../src/FoodStorage.java}\newpage


    \fancyhead[R]{\footnotesize{Menu.pseudocode}}
    \lstinputlisting{../pseudo/Menu.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Menu.java}}
    \lstinputlisting[language=Java]{../src/Menu.java}\newpage


    \fancyhead[R]{\footnotesize{Input.pseudocode}}
    \lstinputlisting{../pseudo/Input.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Input.java}}
    \lstinputlisting[language=Java]{../src/Input.java}\newpage


    \fancyhead[R]{\footnotesize{FileIO.pseudocode}}
    \lstinputlisting{../pseudo/FileIO.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{FileIO.java}}
    \lstinputlisting[language=Java]{../src/FileIO.java}\newpage


    \fancyhead[R]{\footnotesize{Storage}}
    \lstinputlisting{../pseudo/Storage.pseudocode}
    \lstinputlisting[language=Java]{../src/Storage.java}\newpage


    \fancyhead[R]{\footnotesize{Freezer.pseudocode}}
    \lstinputlisting{../pseudo/Freezer.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Freezer.java}}
    \lstinputlisting[language=Java]{../src/Freezer.java}\newpage


    \fancyhead[R]{\footnotesize{Fridge.pseudocode}}
    \lstinputlisting{../pseudo/Fridge.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Fridge.java}}
    \lstinputlisting[language=Java]{../src/Fridge.java}\newpage


    \fancyhead[R]{\footnotesize{Pantry.pseudocode}}
    \lstinputlisting{../pseudo/Pantry.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Pantry.java}}
    \lstinputlisting[language=Java]{../src/Pantry.java}\newpage


    \fancyhead[R]{\footnotesize{Food.pseudocode}}
    \lstinputlisting{../pseudo/Food.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Food.java}}
    \lstinputlisting[language=Java]{../src/Food.java}\newpage


    \fancyhead[R]{\footnotesize{IFood}}
    \lstinputlisting{../pseudo/IFood.pseudocode}
    \lstinputlisting[language=Java]{../src/IFood.java}\newpage


    \fancyhead[R]{\footnotesize{Meat.pseudocode}}
    \lstinputlisting{../pseudo/Meat.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Meat.java}}
    \lstinputlisting[language=Java]{../src/Meat.java}\newpage


    \fancyhead[R]{\footnotesize{Grain.pseudocode}}
    \lstinputlisting{../pseudo/Grain.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Grain.java}}
    \lstinputlisting[language=Java]{../src/Grain.java}\newpage


    \fancyhead[R]{\footnotesize{Fruit.pseudocode}}
    \lstinputlisting{../pseudo/Fruit.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Fruit.java}}
    \lstinputlisting[language=Java]{../src/Fruit.java}\newpage


    \fancyhead[R]{\footnotesize{Vegetable.pseudocode}}
    \lstinputlisting{../pseudo/Vegetable.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Vegetable.java}}
    \lstinputlisting[language=Java]{../src/Vegetable.java}\newpage


    \fancyhead[R]{\footnotesize{Date.pseudocode}}
    \lstinputlisting{../pseudo/Date.pseudocode}\newpage

    \fancyhead[R]{\footnotesize{Date.java}}
    \lstinputlisting[language=Java]{../src/Date.java}\newpage

\end{document}
