CLASS: FoodManager
CLASS FIELDS:
    none

main:
    runMenu <-- none


SUBMODULES:

SUBMODULE runMenu
IMPORTS: none
EXPORTS: none
ASSERTION: Should only exit when use selects exit
ALGORITHM:
    CONSTUCT foodStorage USING FoodStorage CLASS WITH
        Default Constructor

    menuOptions := INITIALISE String[8];
    menuOptions[0] := "Please select an option:"
    menuOptions[1] := "    1. Add food"
    menuOptions[2] := "    2. Remove food"
    menuOptions[3] := "    3. Display contents"
    menuOptions[4] := "    4. Find expired"
    menuOptions[5] := "    5. Read in storage"
    menuOptions[6] := "    6. Write out storage"
    menuOptions[7] := "    7. Exit"

    WHILE (NOT exit) DO
        TRY
            CASE (Menu.getUserInput <-- menuOptions)
                1:
                    foodStorage := addFood <-- foodStorage
                    exit := false
                2:
                    foodStorage := removeFood <-- foodStorage
                    exit := false
                3:
                    displayContents <-- foodStorage
                    exit := false
                4:
                    findExpired <-- foodStorage
                    exit := false
                5:
                    foodStorage := readStorage <-- foodStorage
                    exit := false
                6:
                    writeStorage <-- foodStorage
                    exit := false
                7:
                    OUTPUT "Exiting..."
                    exit := true
            END CASE
        CATCH (IllegalArgumentException e)
            OUTPUT e.getMessage <-- none
            exit := false

    END WHILE


SUBMODULE addFood
IMPORTS: foodStorage (FoodStorage)
EXPORTS: foodStorage (FoodStorage)
ASSERTION: Should add food to storage facility if user inputs are correct
ALGORITHM:
    foodMenuOptions := INITIALISE String[6];

    IF (NOT foodStorage.getConstructFromFile <-- none) THEN
        THROW IllegalArgumentException <-- "Invalid Storage: Cannot add food until read from file"
    ELSE
        validFood := false
        inExit := false

        foodMenuOptions[0] := "Please select food type:"
        foodMenuOptions[1] := "    1. Meat"
        foodMenuOptions[2] := "    2. Grain"
        foodMenuOptions[3] := "    3. Fruit"
        foodMenuOptions[4] := "    4. Vegetable"
        foodMenuOptions[5] := "    5. Exit to main menu"

        WHILE ((NOT inExit) AND (NOT validFood)) DO
            TRY
                CASE (Menu.getUserInput <-- foodMenuOptions)
                    1:
                        choice := "Add meat"
                        newFood := createMeat <-- none
                        foodStorage.addItem <-- newFood
                        validFood := true
                    2:
                        choice := "Add grain"
                        newFood := createGrain <-- none
                        foodStorage.addItem <-- newFood
                        validFood := true
                    3:
                        choice := "Add fruit"
                        newFood := createFruit <-- none
                        foodStorage.addItem <-- newFood
                        validFood := true
                    4:
                        choice := "Add vegetable"
                        newFood := createVegetable <-- none
                        foodStorage.addItem <-- newFood
                        validFood := true
                    5:
                        choice := "Exit"
                        inExit := true
                END CASE
            CATCH (IllegalArgumentException e)
                OUTPUT e.getMessage <-- none
                inExit := false

        END WHILE

        outputMsg := "==============================\n"
                   + "Finish action: " + choice + "\n"
                   + "==============================\n"
        OUTPUT outputMsg
    END IF


SUBMODULE createMeat
IMPORTS: none
EXPORTS: newMeat (Food)
ASSERTION: Should successfully create meat object when user inputs are valid
ALGORITHM:
    prompt := INITIALISE String[6]
    errorMsg := INITIALISE String[6]

    prompt[0] := "Enter meat name:"
    prompt[1] := "Enter meat cut:"
    prompt[2] := "Enter meat weight"
    prompt[3] := "Enter meat storage temperature:"
    prompt[4] := "Enter use by date (dd/mm/yyyy):"
    prompt[5] := "Enter meat package type:"

    errorMsg[0] := "Error: Name is not valid\nPlease try again"
    errorMsg[1] := "Error: Cut is not valid\nPlease try again"
    errorMsg[2] := "Error: Weight is not valid\nPlease try again"
    errorMsg[3] := "Error: Storage temperature is not valid\nPlease try again"
    errorMsg[4] := "Error: Date is not valid\nPlease try again"
    errorMsg[5] := "Error: Package type is not valid\nPlease try again"

    name := Input.string <-- prompt[0], errorMsg[0]
    cut := Input.string <-- prompt[1], errorMsg[1]
    weight := Input.real <-- prompt[2], errorMsg[2], 0.2, 5.0
    tempStorage := Input.real <-- prompt[3], errorMsg[3]

    DO
        TRY
            CONSTRUCT useBy USING Date CLASS WITH
                Input.string <-- prompt[4], errorMsg[4]
            dateValid := true
        CATCH (IllegalArgumentException e)
            OUTPUT errorMsg[4]
            dateValid := false

    WHILE (NOT dateValid)

    packageType := Input.string <-- prompt[5], errorMsg[5]

    CONSTRUCT newMeat USING Meat CLASS WITH
        name, tempStorage, packageType, cut, weight, useBy


SUBMODULE createGrain
IMPORTS: none
EXPORTS: newGrain (Food)
ASSERTION: Should successfully create grain object when user inputs are valid
ALGORITHM:
    prompt := INITIALISE String[6]
    errorMsg := INITIALISE String[6]

    prompt[0] := "Enter grain name:"
    prompt[1] := "Enter grain type:"
    prompt[2] := "Enter grain volume"
    prompt[3] := "Enter grain storage temperature:"
    prompt[4] := "Enter expire by date (dd/mm/yyyy):"
    prompt[5] := "Enter grain package type:"

    errorMsg[0] := "Error: Name is not valid\nPlease try again"
    errorMsg[1] := "Error: Type is not valid\nPlease try again"
    errorMsg[2] := "Error: Volume is not valid\nPlease try again"
    errorMsg[3] := "Error: Storage temperature is not valid\nPlease try again"
    errorMsg[4] := "Error: Date is not valid\nPlease try again"
    errorMsg[5] := "Error: Package type is not valid\nPlease try again"

    name := Input.string <-- prompt[0], errorMsg[0]
    type := Input.string <-- prompt[1], errorMsg[1]
    volume := Input.real <-- prompt[2], errorMsg[2], 0.2, 5.0
    tempStorage := Input.real <-- prompt[3], errorMsg[3]

    DO
        TRY
            CONSTRUCT expireBy USING Date CLASS WITH
                Input.string <-- prompt[4], errorMsg[4]
            dateValid := true
        CATCH (IllegalArgumentException e)
            OUTPUT errorMsg[4]
            dateValid := false

    WHILE (NOT dateValid)

    packageType := Input.string <-- prompt[5], errorMsg[5]

    CONSTRUCT newGrain USING Grain WITH
        name, tempStorage, packageType, type, volume, expireBy


SUBMODULE createFruit
IMPORTS: none
EXPORTS: newFruit (Food)
ASSERTION: Should successfully create fruit object when user inputs are valid
ALGORITHM:
    prompt := INITIALISE String[6]
    errorMsg := INITIALISE String[6]

    prompt[0] := "Enter fruit name:"
    prompt[1] := "Enter fruit type:"
    prompt[2] := "Enter number of pieces"
    prompt[3] := "Enter fruit storage temperature:"
    prompt[4] := "Enter use by date (dd/mm/yyyy):"
    prompt[5] := "Enter fruit package type:"

    errorMsg[0] := "Error: Name is not valid\nPlease try again"
    errorMsg[1] := "Error: Type is not valid\nPlease try again"
    errorMsg[2] := "Error: No. of pieces is not valid\nPlease try again"
    errorMsg[3] := "Error: Storage temperature is not valid\nPlease try again"
    errorMsg[4] := "Error: Date is not valid\nPlease try again"
    errorMsg[5] := "Error: Package type is not valid\nPlease try again"

    name := Input.string <-- prompt[0], errorMsg[0]
    type := Input.string <-- prompt[1], errorMsg[1]
    numPieces := Input.integer <-- prompt[2], errorMsg[2], 1, 20
    tempStorage := Input.real <-- prompt[3], errorMsg[3]

    DO
        TRY
            CONSTRUCT useBy USING Date CLASS WITH
                Input.string <-- prompt[4], errorMsg[4]
            dateValid := true
        CATCH (IllegalArgumentException e)
            OUTPUT errorMsg[4]
            dateValid := false

    WHILE (NOT dateValid)

    packageType := Input.string <-- prompt[5], errorMsg[5]

    CONSTRUCT newFruit USING Fruit WITH
        name, tempStorage, packageType, type, numPieces, useBy


SUBMODULE createVegetable
IMPORTS: none
EXPORTS: newVegetable (Food)
ASSERTION: Should successfully crete vegetable object when user inputs are valid
ALGORITHM:
    prompt := INITIALISE String[5]
    errorMsg := INITIALISE String[5]

    prompt[0] := "Enter vegetable name:"
    prompt[1] := "Enter vegetable weight"
    prompt[2] := "Enter vegetable storage temperature:"
    prompt[3] := "Enter expire by date (dd/mm/yyyy):"
    prompt[4] := "Enter vegetable package type:"

    errorMsg[0] := "Error: Name is not valid\nPlease try again"
    errorMsg[1] := "Error: Weight is not valid\nPlease try again"
    errorMsg[2] := "Error: Storage temperature is not valid\nPlease try again"
    errorMsg[3] := "Error: Date is not valid\nPlease try again"
    errorMsg[4] := "Error: Package type is not valid\nPlease try again"

    name := Input.string <-- prompt[0], errorMsg[0]
    weight := Input.real <-- prompt[1], errorMsg[1], 0.2, 5.0
    tempStorage := Input.real <-- prompt[2], errorMsg[2]

    DO
        TRY
            CONSTRUCT expireBy USING Date CLASS WITH
                Input.string <-- prompt[4], errorMsg[4]
            dateValid := true
        CATCH (IllegalArgumentException e)
            OUTPUT errorMsg[3]
            dateValid := false

    WHILE (NOT dateValid)

    packageType := Input.string <-- prompt[4], errorMsg[4]

    CONSTRUCT newVegetable USING Vegetable WITH
        name, tempStorage, packageType, weight, expireBy


SUBMODULE removeFood
IMPORTS: foodStorage (FoodStorage)
EXPORTS: foodStorage (FoodStorage)
ASSERTION: Should successfully remove food unless it DOesnt exist
ALGORITHM:
    IF (NOT foodStorage.getConstructFromFile <-- none) THEN
        THROW IllegalArgumentException <-- "Invalid Storage: Cannot remove food until read from file"
    ELSE
        prompt := "Enter the index of the item in the storage you want to remove (x,y):\n"
                + "To go back to main menu, enter \"e\""
        errorMsg := "Error: Location entered is not valid\nPlease try again"

        location := Input.string <-- prompt, errorMsg

        IF (NOT location.equals <-- "e") THEN
            outputMsg := foodStorage.removeItem <-- location

            outputMsg += "==============================\n"
                       + "Finish action: Remove item\n"
                       + "==============================\n"
            OUTPUT outputMsg
        END IF
    END IF


SUBMODULE displayContents
IMPORTS: foodStorage (FoodStorage)
EXPORTS: none
ASSERTION: Should exit if storage facility cannot be found
ALGORITHM:
    IF (NOT foodStorage.getConstructFromFile <-- none) THEN
        THROW IllegalArgumentException <-- "Invalid Storage: Cannot display contents until read from file"
    ELSE
        prompt := "Enter the name of the storage facility to display:"
        errorMsg := "Error: Name entered is not valid\n"
                  + "Please try again"

        storageName := Input.string <-- prompt, errorMsg
        toDisplay := foodStorage.displayStorage <-- storageName
        OUTPUT toDisplay

        outputMsg := "==============================\n"
                   + "Finish action: Display contents\n"
                   + "==============================\n"
        OUTPUT outputMsg
    END IF


SUBMODULE findExpired
IMPORTS: foodStorage (FoodStorage)
EXPORTS: none
ASSERTION: Display expired food and return to main menu
ALGORITHM:
    IF (NOT foodStorage.getConstructFromFile <-- none) THEN
        THROW IllegalArgumentException <-- "Invalid Storage: Cannot find expired until read from file"
    ELSE
        OUTPUT foodStorage.findExpired <-- none

        outputMsg := "==============================\n"
                   + "Finish action: Find expired items\n"
                   + "==============================\n"
        OUTPUT outputMsg
    END IF


SUBMODULE readStorage
IMPORTS: foodStorage (FoodStorage)
EXPORTS: foodStorage (FoodStorage)
ASSERTION: Should be able to construct storage facility unless file is not a valid format
ALGORITHM:
    IF (foodStorage.getConstructFromFile <-- none) THEN
        THROW IllegalArgumentException <-- "Invalid Storage: Already read from file"
    ELSE
        prompt := "Enter the filename to read from:"
        errorMsg := "Error: Filename entered is not valid\n"
                  + "Please try again"

        filename := Input.string <-- prompt, errorMsg
        fileContents := FileIO.readFileContents <-- filename

        foodStorage := processStorage <-- fileContents, foodStorage
        foodStorage := processFood <-- fileContents, foodStorage

        outputMsg := "==============================\n"
                   + "Finish action: Read storage\n"
                   + "==============================\n"
        OUTPUT outputMsg
    END IF


SUBMODULE processStorage
IMPORTS: inFileContents (String array), foodStorage (FoodStorage)
EXPORTS: foodStorage (FoodStorage)
ASSERTION: Should be able to create storage facility unless file is not a valid format
ALGORITHM:
    IF (LENGTH OF inFileContents < 3) THEN
        THROW IllegalArgumentException <-- "Invalid Storage: Input file DOes not contain enough information to construct storage facility"
    ELSE
        newStorage := INITIALISE Storage[3][]

        FOR i := 0 TO 3 EXCLUSIVE EXCLUSIVE CHANGEBY 1
            splitLine := inFileContents[i].split <-- ","

            IF (LENGTH OF splitLine < 1) THEN
                THROW IllegalArgumentException <-- "Invalid Storage: Input file DOes not contain enough information to construct storage facility"
            ELSE
                storageType := splitLine[0]
                storageSize := Integer.parseInt <-- splitLine[1]

                IF (storageType.equals <-- "Freezer") THEN
                    newStorage[i] := INITIALISE Freezer[storageSize]
                    FOR j := 0 TO LENGTH OF newStorage[i] EXCLUSIVE EXCLUSIVE CHANGEBY 1
                        CONSTRUCT newStorage[i][j] USING Freezer CLASS WITH
                            storageType
                    END FOR
                ELSE IF (storageType.equals <-- "Fridge") THEN
                    newStorage[i] := INITIALISE Fridge[storageSize]
                    FOR j := 0 TO LENGTH OF newStorage[i] EXCLUSIVE EXCLUSIVE CHANGEBY 1
                        CONSTRUCT newStorage[i][j] USING Fridge CLASS WITH
                            storageType
                    END FOR
                ELSE IF (storageType.equals <-- "Pantry") THEN
                    newStorage[i] := INITIALISE Pantry[storageSize]
                    FOR j := 0 TO LENGTH OF newStorage[i] EXCLUSIVE EXCLUSIVE CHANGEBY 1
                        CONSTRUCT newStorage[i][j] USING Pantry CLASS WITH
                            storageType
                    END FOR
                END IF
            END IF
        END FOR
        CONSTRUCT foodStorage USING FoodStorage CLASS WITH
            newStorage
    END IF


SUBMODULE processFood
IMPORTS: inFileContents (String array), foodStorage (FoodStorage)
EXPORTS: foodStorage (FoodStorage)
ASSERTION: Create an array of food objects, skipping invalid entries
ALGORITHM:
    FOR i := 0 TO LENGTH OF inFileContents EXCLUSIVE EXCLUSIVE CHANGEBY 1
        TRY
            splitLine := inFileContents[i].split <-- ","

            IF (LENGTH OF splitLine > 2) THEN
                typeFood := splitLine[0]

                IF (typeFood.equals <-- "Meat") THEN
                    name := splitLine[1]
                    cut := splitLine[2]
                    weight := Double.parseDouble <-- splitLine[3]
                    tempStorage := Double.parseDouble <-- splitLine[4]
                    CONSTRUCT useBy USING Date WITH
                        splitLine[5]
                    packageType := splitLine[6]
                    CONSTRUCT newMeat USING Meat CLASS WITH
                        name, tempStorage, packageType, cut, weight, useBy
                    foodStorage.addItem <-- newMeat
                    countValid += 1
                ELSE IF (typeFood.equals <-- "Grain") THEN
                    name := splitLine[1]
                    type := splitLine[2]
                    volume := Double.parseDouble <-- splitLine[3]
                    tempStorage := Double.parseDouble <-- splitLine[4]
                    CONSTRUCT expireBy USING Date WITH
                        splitLine[5]
                    packageType := splitLine[6]
                    CONSTRUCT newGrain USING Grain WITH
                        name, tempStorage, packageType, type, volume, expireBy
                    foodStorage.addItem <-- newGrain
                    countValid += 1
                ELSE IF (typeFood.equals <-- "Fruit") THEN
                    name := splitLine[1]
                    type := splitLine[2]
                    numPieces := Integer.parseInt <-- splitLine[3]
                    tempStorage := Double.parseDouble <-- splitLine[4]
                    CONSTRUCT useBy USING Date WITH
                        splitLine[5]
                    packageType := splitLine[6]
                    CONSTRUCT newFruit USING Fruit WITH
                        name, tempStorage, packageType, type, numPieces, useBy
                    foodStorage.addItem <-- newFruit
                    countValid += 1
                ELSE IF (typeFood.equals <-- "Vegetable") THEN
                    name := splitLine[1]
                    weight := Double.parseDouble <-- splitLine[2]
                    tempStorage := Double.parseDouble <-- splitLine[3]
                    CONSTRUCT expireBy USING Date WITH
                        splitLine[4]
                    packageType := splitLine[5]
                    CONSTRUCT newVegetable USING Vegetable WITH
                        name, tempStorage, packageType, weight, expireBy
                    foodStorage.addItem <-- newVegetable
                    countValid += 1
                END IF
            END IF
        CATCH (IllegalArgumentException e)
            countInvalid += 1
            OUTPUT e.getMessage <-- none
            OUTPUT "Entry at line " + (i + 1) + " is invalid, skipping...\n"

    END FOR

    outputMsg := "No. of valid entries: " + countValid + "\n"
               + "No. of invalid entries: " + countInvalid
    OUTPUT outputMsg


SUBMODULE writeStorage
IMPORTS: foodStorage (FoodStorage)
EXPORTS: none
ASSERTION: Export storage and food objects to a file in csv form
ALGORITHM:
    IF (NOT foodStorage.getConstructFromFile <-- none) THEN
        THROW IllegalArgumentException <-- "Invalid Storage: Cannot write to file until read from file"
    ELSE
        prompt := "Enter the filename to write to:"
        errorMsg := "Error: Filename entered is not valid\n"
                  + "Please try again"

        outFilename := Input.string <-- prompt, errorMsg

        outFileContents := foodStorage.exportFoodStorage <-- none
        FileIO.saveFileContents <-- outFilename, outFileContents

        outputString := "==============================\n"
                      + "Successfully saved " + LENGTH OF outFileContents + " lines\n"
                      + "==============================\n\n"
        OUTPUT outputString
    END IF

