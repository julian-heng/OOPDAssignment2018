CLASS: Freezer
EXTENDS: Storage

CLASS FIELDS:
    LOWERTEMP := -27.0 (double Constant)
    UPPERTEMP := -5.0 (double Constant)
    name (String)
    item (Food)
    hasItem (boolean)


SUBMODULES:

CONSTRUCTORS:

SUBMODULE Default Constructor
IMPORTS: none
EXPORTS: none
ASSERTION: Freezer object with default values is created
ALGORITHM:
    name := "Placeholder"
    CONSTUCT item USING Meat CLASS WITH
        Default CONSTRUCTOR


SUBMODULE Alternate Constructor (No Item)
IMPORTS: inName (String)
EXPORTS: none
ASSERTION: Freezer object with import value is created
ALGORITHM:
    setName <-- inName
    CONSTUCT item USING Meat CLASS WITH
        Default CONSTRUCTOR


SUBMODULE Alternate Constructor (Item)
IMPORTS: inName (String), inItem (Food)
EXPORTS: none
ASSERTION: Freezer object with import value is created
ALGORITHM:
    setName <-- inName
    setItem <-- inItem


SUBMODULE Copy Constructor
IMPORTS: inFreezer (Freezer)
EXPORTS: none
ASSERTION: Copy of inFreezer will be created
ALGORITHM:
    name := inFreezer.getName <-- none
    item := inFreezer.getItem <-- none
    hasItem := inFreezer.getHasItem <-- none


ACCESSORS:

SUBMODULE isExpired
IMPORTS: none
EXPORTS: isExpired (boolean)
ASSERTION: Return true or false
ALGORITHM:
    CONSTRUCT today USING Date CLASS WITH
        setToday METHOD
    isExpired := (item.calcExpire <-- today)


SUBMODULE toCSV OVERRIDE STORAGE
IMPORTS: none
EXPORTS: itemCSV
ASSERTION: Call the toCSV method in the item and pass it through
ALGORITHM:
    itemCSV := item.toCSV <-- none


SUBMODULE canAdd OVERRIDE STORAGE
IMPORTS: inFood (Food)
EXPORTS: add (boolean)
ASSERTION: Returns true or false
ALGORITHM:
    IF (NOT validateObject(inFood)) THEN
        THROW IllegalArgumentException <-- "Invalid Freezer: Item is not valid, cannot check if can be added"
    ELSE
        add := (validateTemp <-- inFood)
    END IF


SUBMODULE setName
IMPORTS: inName (String)
EXPORTS: none
ASSERTION: name will be replaced by inName
ALGORITHM:
    IF (NOT validateString <-- inName) THEN
        THROW IllegalArgumentException <-- "Invalid Freezer: Name is either null or empty"
    ELSE
        name := inName
    END IF


SUBMODULE setItem OVERRIDE STORAGE
IMPORTS: inItem (Food)
EXPORTS: none
ASSERTION: item will be replaced by inItem
ALGORITHM:
    IF (NOT validateObject <-- inItem) THEN
        THROW IllegalArgumentException <-- "Invalid Freezer: Item is not valid"
    ELSE IF (NOT validateTemp <-- inItem) THEN
        THROW IllegalArgumentException <-- "Invalid Freezer: Item cannot be stored, temperature is out of range"
    ELSE
        item := inItem.clone <-- none
        hasItem := true
    END IF


ACCESSORS:

SUBMODULE getLower OVERRIDE STORAGE
IMPORTS: none
EXPORTS: LOWERTEMP (double)
ASSERTION: Return LOWERTEMP


SUBMODULE getUpper OVERRIDE STORAGE
IMPORTS: none
EXPORTS: UPPERTEMP (double)
ASSERTION: Return UPPERTEMP


SUBMODULE getName OVERRIDE STORAGE
IMPORTS: none
EXPORTS: name (String)
ASSERTION: Return name


SUBMODULE getItem OVERRIDE STORAGE
IMPORTS: none
EXPORTS: item clone (Food)
ASSERTION: Return clone of item in Freezer


SUBMODULE getHasItem OVERRIDE STORAGE
IMPORTS: none
EXPORTS: hasItem (boolean)
ASSERTION: Return true or false


SUBMODULE equals
IMPORTS: inObj (Object)
EXPORTS: isEquals (boolean)
ASSERTION: Return true or false
ALGORITHM:
    IF (inObj ISA Freezer) THEN
        Freezer inFreezer := (Freezer)inObj
        isEquals := (
            (
                realCompare <-- LOWERTEMP, inFreezer.getLower <-- none
            ) AND (
                realCompare <-- UPPERTEMP, inFreezer.getUpper <-- none
            ) AND (
                name.equals <-- inFreezer.getName <-- none
            ) AND (
                item.equals <-- inFreezer.getItem <-- none
            )
        )
    END IF


SUBMODULE toString
IMPORTS: none
EXPORTS: str (String)
ASSERTION: Return a string of all the variables in Freezer as well as item
ALGORITHM:
    str := "Storage type: " + name + "\n"
        + "Lower storage temp: " + LOWERTEMP + "\n"
        + "Upper storage temp: " + UPPERTEMP + "\n"
        + item.toString <-- none


SUBMODULE clone OVERRIDE STORAGE
IMPORTS: none
EXPORTS: cloneFreezer (Freezer)
ASSERTION: A clone will be made
ALGORITHM:
    CONSTRUCT cloneFreezer USING Freezer CLASS WITH
        this


SUBMODULE placeholder OVERRIDE STORAGE
IMPORTS: none
EXPORTS: placeholder (Freezer)
ASSERTION: Placeholder Freezer object will be created
ALGORITHM:
    CONSTRUCT placeholder USING Freezer CLASS WITH
        "Freezer"


PRIVATE SUBMODULES:

SUBMODULE validateString
IMPORTS: inString (String)
EXPORTS: isValid (boolean)
ASSERTION: Return true or false
ALGORITHM:
    isValid := ((NOT inString == null) AND (NOT inString.isEmpty <-- none))


SUBMODULE validateObject
IMPORTS: inObj (Object)
EXPORTS: isValid (boolean)
ASSERTION: Return true or false
ALGORITHM:
    isValid := (NOT inObj == null)


SUBMODULE validateTemp
IMPORTS: inItem (Food)
EXPORTS: isValid (boolean)
ASSERTION: Return true or false
ALGORITHM:
    isValid := (
        (
            (inItem.getTempStorage <-- none > LOWERTEMP) OR (realCompare <-- inItem.getTempStorage <-- none, LOWERTEMP)
        ) AND (
            (inItem.getTempStorage <-- none < UPPERTEMP) OR (realCompare <-- inItem.getTempStorage <-- none, UPPERTEMP)
        )
    )


SUBMODULE realCompare
IMPORTS: num1 (double), num2 (double)
EXPORTS: isEquals (boolean)
ASSERTION: Return true or false
ALGORITHM:
    isEquals := (Math.abs(num1 - num2) < 0.0000001)

